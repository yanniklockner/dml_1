6x2 mal3

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0               254               254               244               264 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       255.3252258               254               244               264 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       123.6100922                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0                 0                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       511.6083374                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0        1539.95459                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       1660.205811                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       9.008705139                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       16.00896835                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       13.91628742                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0        173.003952                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0       8.521929741                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0       2.179373503                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0        2.27600503                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       524.7372214               540       537.2999878       542.7000122 Laenge # Laenge [mm]
  3320      2       173.1075484               180       179.1000061       180.8999939 Breite # Breite [mm]
  3321      3       88.40915307        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4       86.54144287      0.1200752258      0.1080677062      0.1320827454 Rundheit # Rundheit [mm]
  3320      5       176.9316736       182.6309967       181.7180023       183.5440063 Hoehe # Hoehe [mm]
  3350      0          2299.180                 0                 0                 0 Bauteilgewicht [g]