1x1 mal3

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0               240               240               230               250 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       240.5855713               240               230               250 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       62.21360016                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0   0.0004527277197                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       891.9335938                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       891.8641357                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       484.8041077                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       2.008942604                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       11.05710793                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       8.031076431                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       109.8395157                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0       1.991897225                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.5582909584                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0       1.647322059                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       88.11644634                90       89.55000305       90.44999695 Laenge # Laenge [mm]
  3320      2       88.14170582                90       89.55000305       90.44999695 Breite # Breite [mm]
  3321      3       89.01107404        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4     0.03383255005      0.0135307312     0.01217765827     0.01488380414 Rundheit # Rundheit [mm]
  3320      5       179.0457844       182.6309967       181.7180023       183.5440063 Hoehe # Hoehe [mm]
  3350      0           281.018                 0                 0                 0 Bauteilgewicht [g]