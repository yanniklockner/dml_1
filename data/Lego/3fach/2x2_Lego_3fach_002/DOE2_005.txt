2x2 mal3

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0               260               260               250               270 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       260.7345581               260               250               270 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       73.54031372                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0                 0                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       1041.186523                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       1040.880127                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       1129.858154                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       4.076606274                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       13.82869434                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       10.90170288                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       120.6046829                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0       3.669123173                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0       1.580647588                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0       1.918214798                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1        174.198674               180       179.1000061       180.8999939 Laenge # Laenge [mm]
  3320      2       174.3250497               180       179.1000061       180.8999939 Breite # Breite [mm]
  3321      3       88.26785593        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4      0.1809883118     0.04378128052     0.03940315172     0.04815940931 Rundheit # Rundheit [mm]
  3320      5       177.9244326       182.6309967       181.7180023       183.5440063 Hoehe # Hoehe [mm]
  3350      0           834.480                 0                 0                 0 Bauteilgewicht [g]