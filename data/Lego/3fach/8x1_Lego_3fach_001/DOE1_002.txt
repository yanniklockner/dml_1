8x1 mal3

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0               226               226               216               236 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       227.5744781               226               216               236 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       159.0102844                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0       4.231630802                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       254.8236084                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       2046.441162                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       1098.828369                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       5.099321842                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       14.92724419                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       12.00824928                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       263.7609253                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0       8.010548592                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0       5.283156395                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0       1.959975362                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       706.3123812               720       716.4000244       723.5999756 Laenge # Laenge [mm]
  3320      2       77.68537811                90       89.55000305       90.44999695 Breite # Breite [mm]
  3321      3       88.80606452        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4      0.1522941589       0.133354187      0.1200187653      0.1466896087 Rundheit # Rundheit [mm]
  3320      5       178.1039191       182.6309967       181.7180023       183.5440063 Hoehe # Hoehe [mm]
  3350      0          1684.730                 0                 0                 0 Bauteilgewicht [g]