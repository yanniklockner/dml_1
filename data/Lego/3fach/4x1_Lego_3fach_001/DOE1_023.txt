4x1 mal3

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0               254               254               244               264 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       254.7130737               254               244               264 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       76.45510864                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0       3.728362799                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       782.7371826                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       3145.120361                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       1701.621216                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       2.197310209                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0        12.4212532                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       7.804399967                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       295.9454346                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0       3.266227722                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0         2.6529181                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0        1.11207819                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       355.9613337               360       358.2000122       361.7999878 Laenge # Laenge [mm]
  3320      2       83.74493284                90       89.55000305       90.44999695 Breite # Breite [mm]
  3321      3       88.84805232        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4     0.07917785645     0.05405807495     0.04865226895     0.05946388096 Rundheit # Rundheit [mm]
  3320      5       179.9426827       182.6309967       181.7180023       183.5440063 Hoehe # Hoehe [mm]
  3350      0           914.857                 0                 0                 0 Bauteilgewicht [g]