2x1 geteilt3

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0     0.05282230675                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       211.6427917               226               216               236 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       226.0408783               226               216               236 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       166.8487854                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0       1.827874303                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       9.419192314                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       18.74422264                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       10.12143326                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       2.091169596                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       13.16921711                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       3.548936605                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       14.94489574                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0      0.1193804219                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.0382810533                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0      0.7454638481                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       19.84764027                20       19.89999962       20.10000038 Laenge # Laenge [mm]
  3320      2       9.856499734                10       9.949999809       10.05000019 Breite # Breite [mm]
  3321      3        89.7166171        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4    0.007462263107    0.006558418274     0.00590257626    0.007214260288 Rundheit # Rundheit [mm]
  3320      5       20.11293229       20.29199982       20.19099998       20.39299965 Hoehe # Hoehe [mm]
  3350      0             0.708                 0                 0                 0 Bauteilgewicht [g]