1x2 geteilt3

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0     0.02155871317                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       210.8063202               226               216               236 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       226.0449829               226               216               236 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       113.3426132                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0       1.327569962                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       18.84376526                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       9.351458549                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       10.16261768                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       2.216415644                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       13.16921711                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       3.574501753                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       5.983707905                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0      0.1269946545                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0     0.04490654916                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0      0.7475996017                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       9.856269904                10       9.949999809       10.05000019 Laenge # Laenge [mm]
  3320      2       19.84887338                20       19.89999962       20.10000038 Breite # Breite [mm]
  3321      3       89.70445599        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4    0.008284330368    0.007385969162    0.006647372153    0.008124565706 Rundheit # Rundheit [mm]
  3320      5       20.09342406       20.29199982       20.19099998       20.39299965 Hoehe # Hoehe [mm]
  3350      0             0.708                 0                 0                 0 Bauteilgewicht [g]