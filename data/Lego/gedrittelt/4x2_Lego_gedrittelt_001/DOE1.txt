4x2 geteilt3


6 Variablen:

no     id  index      value step_value  name_unit
Nr Kenner  Index       Wert Stufenwert  Bezeichnung [Einheit]
 1   5288      1    5.56064      1.833  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        350        177  Nachdruck 1 - Druck [bar]
 3   3947      0        240         14  Schmelzetemperatur [�C]
 4   3944      0         40         14  Wandtemperatur [�C]
 5   1030      0        9.5        4.6  Nachdruckzeit [s]
 6   3620      0         24        5.7  K�hlzeit im Werkzeug ab F�llende [s]


65 Tests ("-" = Wert - Variation, "0" = Wert, "+" = Wert + Variation):

Nr
 1 - - - - - -
 2 + - - - - -
 3 - + - - - -
 4 + + - - - -
 5 - - + - - -
 6 + - + - - -
 7 - + + - - -
 8 + + + - - -
 9 - - - + - -
10 + - - + - -
11 - + - + - -
12 + + - + - -
13 - - + + - -
14 + - + + - -
15 - + + + - -
16 + + + + - -
17 - - - - + -
18 + - - - + -
19 - + - - + -
20 + + - - + -
21 - - + - + -
22 + - + - + -
23 - + + - + -
24 + + + - + -
25 - - - + + -
26 + - - + + -
27 - + - + + -
28 + + - + + -
29 - - + + + -
30 + - + + + -
31 - + + + + -
32 + + + + + -
33 - - - - - +
34 + - - - - +
35 - + - - - +
36 + + - - - +
37 - - + - - +
38 + - + - - +
39 - + + - - +
40 + + + - - +
41 - - - + - +
42 + - - + - +
43 - + - + - +
44 + + - + - +
45 - - + + - +
46 + - + + - +
47 - + + + - +
48 + + + + - +
49 - - - - + +
50 + - - - + +
51 - + - - + +
52 + + - - + +
53 - - + - + +
54 + - + - + +
55 - + + - + +
56 + + + - + +
57 - - - + + +
58 + - - + + +
59 - + - + + +
60 + + - + + +
61 - - + + + +
62 + - + + + +
63 - + + + + +
64 + + + + + +
65 0 0 0 0 0 0


Test 1:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 2:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 3:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 4:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 5:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 6:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 7:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 8:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 9:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 10:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 11:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 12:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 13:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 14:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 15:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 16:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 17:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 18:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 19:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 20:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 21:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 22:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 23:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 24:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 25:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 26:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 27:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 28:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 29:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 30:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 31:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 32:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       18.3  -  K�hlzeit im Werkzeug ab F�llende [s]


Test 33:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 34:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 35:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 36:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 37:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 38:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 39:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 40:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 41:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 42:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 43:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 44:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 45:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 46:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 47:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 48:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0        4.9  -  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 49:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 50:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 51:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 52:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 53:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 54:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 55:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 56:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         26  -  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 57:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 58:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 59:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 60:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        226  -  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 61:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 62:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        173  -  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 63:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    3.72764  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 64:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    7.39364  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        527  +  Nachdruck 1 - Druck [bar]
 3   3947      0        254  +  Schmelzetemperatur [�C]
 4   3944      0         54  +  Wandtemperatur [�C]
 5   1030      0       14.1  +  Nachdruckzeit [s]
 6   3620      0       29.7  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 65:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    5.56064  0  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        350  0  Nachdruck 1 - Druck [bar]
 3   3947      0        240  0  Schmelzetemperatur [�C]
 4   3944      0         40  0  Wandtemperatur [�C]
 5   1030      0        9.5  0  Nachdruckzeit [s]
 6   3620      0         24  0  K�hlzeit im Werkzeug ab F�llende [s]
