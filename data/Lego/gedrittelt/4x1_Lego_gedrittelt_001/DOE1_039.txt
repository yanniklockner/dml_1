4x1 geteilt3

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0     0.06435143948                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       213.2246552               254               244               264 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0        254.033493               254               244               264 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       190.0893555                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0        1.57758379                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       9.416910172                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       37.39725494                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0        20.1340313                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       2.051695347                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       14.44472218                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       3.592259407                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       6.472480297                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0      0.1659826189                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0     0.07557015866                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0      0.7458145618                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       39.71664019                40       39.79999924       40.20000076 Laenge # Laenge [mm]
  3320      2       9.774521577                10       9.949999809       10.05000019 Breite # Breite [mm]
  3321      3       89.73662651        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4    0.009829044342    0.009970664978     0.00897359848     0.01096773148 Rundheit # Rundheit [mm]
  3320      5       20.13466043       20.29199982       20.19099998       20.39299965 Hoehe # Hoehe [mm]
  3350      0             1.312                 0                 0                 0 Bauteilgewicht [g]