2x2 Lego 0,5xhoch

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       225.3709259               226               216               236 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       226.2116394               226               216               236 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       72.45858765                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0   0.0003539944591                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       27.86699104                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       27.72514343                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       60.93670273                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       4.223117828                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       16.05246735                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       4.736741066                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       19.80157852                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0      0.4654570222                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.1763893813                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0      0.8669331074                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       59.22963773                60       59.70000076       60.29999924 Laenge # Laenge [mm]
  3320      2       59.13969187                60       59.70000076       60.29999924 Breite # Breite [mm]
  3321      3       89.35535895        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4     0.02499389648     0.01954174042     0.01758756675     0.02149591409 Rundheit # Rundheit [mm]
  3320      5       32.66351209       33.09500122       32.93000031       33.25999832 Hoehe # Hoehe [mm]
  3350      0            18.454                 0                 0                 0 Bauteilgewicht [g]