2x2 Lego 0,5xhoch

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       253.8914948               254               244               264 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       254.3578339               254               244               264 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       68.47351074                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0                 0                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       86.01914215                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       85.97389984                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       188.8377075                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       2.255354881                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       13.25656796                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       3.193814993                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       15.76718616                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0      0.5231918693                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.3023983538                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0      0.6925141215                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1        59.0927918                60       59.70000076       60.29999924 Laenge # Laenge [mm]
  3320      2       59.64955058                60       59.70000076       60.29999924 Breite # Breite [mm]
  3321      3       89.57134949        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4     0.03107643127     0.01954174042     0.01758756675     0.02149591409 Rundheit # Rundheit [mm]
  3320      5       32.73480942       33.09500122       32.93000031       33.25999832 Hoehe # Hoehe [mm]
  3350      0            18.749                 0                 0                 0 Bauteilgewicht [g]