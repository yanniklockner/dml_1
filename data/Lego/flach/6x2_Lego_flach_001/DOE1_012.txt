6x2 Lego 0,5xhoch

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       225.7316742               226               216               236 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0        226.817688               226               216               236 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       154.6316833                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0                 0                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       85.70147705                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       257.2072144                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       560.0130005                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       2.757992983                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       12.10704899                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0        3.36119318                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       24.17870331                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0       1.225646257                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.2707333863                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0       1.044189453                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       177.6531471               180       179.1000061       180.8999939 Laenge # Laenge [mm]
  3320      2       59.38825206                60       59.70000076       60.29999924 Breite # Breite [mm]
  3321      3       89.48415102        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4     0.04132652283     0.02129173279     0.01916255988     0.02342090569 Rundheit # Rundheit [mm]
  3320      5       32.67300599       33.09500122       32.93000031       33.25999832 Hoehe # Hoehe [mm]
  3350      0            53.600                 0                 0                 0 Bauteilgewicht [g]