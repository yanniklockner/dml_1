1x2 Lego

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       253.2756042               254               244               264 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       254.0930481               254               244               264 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       52.87288284                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0       1.456966281                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       173.9140472                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       86.78883362                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       94.37815857                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       2.186169624                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       14.23466969                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       2.770247221                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       32.93887711                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0      0.4852387309                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.2773122489                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0      0.7525615692                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       25.70450122                26       25.87000084       26.12999916 Laenge # Laenge [mm]
  3320      2       55.57397699                56       55.72000122       56.27999878 Breite # Breite [mm]
  3321      3       89.83457685        90.0000025       89.00000134       90.99999684 Winkel rechts von Anguss # Winkel rechts von Anguss [�]
  3312      4      0.0196352005     0.01753234863      0.0157791134     0.01928558387 Rundheit # Rundheit [mm]
  3320      5       60.39012395        60.8769989       60.57300186       61.18099976 H�he # H�he [mm]
  3350      0            19.270                 0                 0                 0 Bauteilgewicht [g]