8x2 Lego

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       240.7373047               254               244               264 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       254.0593567               254               244               264 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       168.7592163                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0                 0                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       173.2698669                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       692.8759766                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       743.6353149                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       2.731166601                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       11.40378189                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       3.160616636                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       31.53354073                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0       1.280270934                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.0871604681                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0       1.022887945                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       237.5790072               240       238.8000031       241.1999969 Laenge # Laenge [mm]
  3320      2       59.37275402                60       59.70000076       60.29999924 Breite # Breite [mm]
  3321      3       89.96667262        90.0000025       89.00000134       90.99999684 Winkel rechts von Anguss # Winkel rechts von Anguss [�]
  3312      4     0.04264163971     0.04116153717     0.03704538196     0.04527769238 Rundheit rechts von Anguss # Rundheit rechts von Anguss [mm]
  3320      5       60.28979264        60.8769989       60.57300186       61.18099976 Hoehe rechts von Anguss # Hoehe rechts von Anguss [mm]
  3350      0           126.805                 0                 0                 0 Bauteilgewicht [g]