8x1 Lego

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       253.1002197               254               244               264 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       254.3401184               254               244               264 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       166.3214874                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0       3.209944248                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       28.08585358                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       221.6719513                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       118.9004669                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       4.299355507                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       14.69402218                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       5.359464645                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0        34.0427475                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0       1.144310951                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.7737436891                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0      0.8994169235                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       237.9368272               240       238.8000031       241.1999969 Laenge # Laenge [mm]
  3320      2       28.19078281                30       29.85000038       30.14999962 Breite # Breite [mm]
  3321      3       89.52836845        90.0000025       89.00000134       90.99999684 Winkel rechts von Anguss # Winkel rechts von Anguss [�]
  3312      4     0.02613639832      0.0246629715     0.02219667472     0.02712926827 Rundheit rechts von Anguss # Rundheit rechts von Anguss [mm]
  3320      5       60.25491896        60.8769989       60.57300186       61.18099976 Hoehe rechts von Anguss # Hoehe rechts von Anguss [mm]
  3350      0            67.082                 0                 0                 0 Bauteilgewicht [g]