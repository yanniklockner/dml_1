3x1 Lego

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       252.7362366               254               244               264 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       254.0585938               254               244               264 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       76.08023834                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0       3.143057108                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       86.80649567                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0        261.026947                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       141.3696747                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       2.277966738                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       13.34990978                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       3.411678076                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       34.07593918                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0      0.5768737793                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.2992195189                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0      0.8188773394                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       89.30756959                90       89.55000305       90.44999695 Laenge # Laenge [mm]
  3320      2       29.38412545                30       29.85000038       30.14999962 Breite # Breite [mm]
  3321      3       89.55141296       91.85400271       90.85400155       92.85399705 Winkel rechts von Anguss # Winkel rechts von Anguss [�]
  3312      4     0.01453113556    0.009115219116    0.008203697391     0.01002674084 Rundheit rechts von Anguss # Rundheit rechts von Anguss [mm]
  3320      5       60.32330105        60.8769989       60.57300186       61.18099976 Hoehe rechts von Anguss # Hoehe rechts von Anguss [mm]
  3350      0            27.345                 0                 0                 0 Bauteilgewicht [g]