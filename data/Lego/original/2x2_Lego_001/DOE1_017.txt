2x2 Lego

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       224.9358215               226               216               236 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       226.5204926               226               216               236 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       105.9505692                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0                 0                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       55.44673538                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       55.84330368                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       60.11285019                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       4.343921661                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       16.24466896                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       4.820062637                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       20.74692726                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0      0.5535447001                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.1835595816                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0      0.9751622081                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       59.17340048                60       59.70000076       60.29999924 Laenge # Laenge [mm]
  3320      2       59.14515228                60       59.70000076       60.29999924 Breite # Breite [mm]
  3321      3       89.43443619        90.0000025       89.00000134       90.99999684 Winkel rechts von Anguss # Winkel rechts von Anguss [�]
  3312      4       19.59449768     0.03500000015     0.03200000152      0.0390000008 Rundheit rechts von Anguss # Rundheit rechts von Anguss [mm]
  3320      5       60.14168372        60.8769989       60.57300186       61.18099976 Hoehe rechts von Anguss # Hoehe rechts von Anguss [mm]
  3350      0            33.045                 0                 0                 0 Bauteilgewicht [g]