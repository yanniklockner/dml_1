4x2 Lego

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0        220.118454               226               216               236 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       226.1018372               226               216               236 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       138.6793518                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0                 0                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       55.05276489                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       116.1681366                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       107.7564163                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       4.365718365                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       14.74713898                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       5.046592712                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       15.81377125                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0      0.8429463506                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.2831850648                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0      0.8816130757                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       118.5984728               120       119.4000015       120.5999985 Laenge # Laenge [mm]
  3320      2       59.25032044                60       59.70000076       60.29999924 Breite # Breite [mm]
  3321      3       89.50069115        90.0000025       89.00000134       90.99999684 Winkel rechts von Anguss # Winkel rechts von Anguss [�]
  3312      4      0.0283203125     0.01876735687     0.01689062081     0.02064409293 Rundheit rechts von Anguss # Rundheit rechts von Anguss [mm]
  3320      5       60.19910736        60.8769989       60.57300186       61.18099976 Hoehe rechts von Anguss # Hoehe rechts von Anguss [mm]
  3350      0            63.418                 0                 0                 0 Bauteilgewicht [g]