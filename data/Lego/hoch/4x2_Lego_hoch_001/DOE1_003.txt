4x2 Lego 2xhoch

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       214.6433868               226               216               236 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       226.0771179               226               216               236 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       190.4636688                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0                 0                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       283.3130188                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       367.6759644                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       563.2203369                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       2.433887005                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       11.81824017                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0        2.99159956                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0        16.1957531                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0      0.6175807118                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.1099604145                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0      0.7615794539                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       119.0892403               120       119.4000015       120.5999985 Laenge # Laenge [mm]
  3320      2       59.50664855                60       59.70000076       60.29999924 Breite # Breite [mm]
  3321      3       89.89370679        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4     0.02140426636      0.0162820816     0.01465387363     0.01791029051 Rundheit # Rundheit [mm]
  3320      5       95.42185471       96.17700195       95.69599915       96.65799713 Hoehe # Hoehe [mm]
  3350      0            97.641                 0                 0                 0 Bauteilgewicht [g]