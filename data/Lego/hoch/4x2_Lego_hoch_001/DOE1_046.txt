4x2 Lego 2xhoch

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       249.4793854               254               244               264 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       254.2740479               254               244               264 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       156.0348053                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0                 0                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       92.38265991                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       119.8476868                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       183.6100464                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       4.833518982                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0         16.711092                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       6.308064461                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       27.09000587                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0       1.389627695                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.5535925031                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0       1.316466212                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       117.9629287               120       119.4000015       120.5999985 Laenge # Laenge [mm]
  3320      2       59.09894342                60       59.70000076       60.29999924 Breite # Breite [mm]
  3321      3       89.39680877        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4     0.03038024902      0.0162820816     0.01465387363     0.01791029051 Rundheit # Rundheit [mm]
  3320      5       94.74295642       96.17700195       95.69599915       96.65799713 Hoehe # Hoehe [mm]
  3350      0            94.285                 0                 0                 0 Bauteilgewicht [g]