8x2 Lego 2xhoch


6 Variablen:

no     id  index      value step_value  name_unit
Nr Kenner  Index       Wert Stufenwert  Bezeichnung [Einheit]
 1   5288      1   149.9974         70  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        350        250  Nachdruck 1 - Druck [bar]
 3   3947      0        240         20  Schmelzetemperatur [�C]
 4   3944      0         40         20  Wandtemperatur [�C]
 5   1030      0        9.5        6.5  Nachdruckzeit [s]
 6   3620      0         24      7.989  K�hlzeit im Werkzeug ab F�llende [s]


12 Tests ("-" = Wert - Variation, "0" = Wert, "+" = Wert + Variation):

Nr
 1 + 0 0 0 0 0
 2 - 0 0 0 0 0
 3 0 + 0 0 0 0
 4 0 - 0 0 0 0
 5 0 0 + 0 0 0
 6 0 0 - 0 0 0
 7 0 0 0 + 0 0
 8 0 0 0 - 0 0
 9 0 0 0 0 + 0
10 0 0 0 0 - 0
11 0 0 0 0 0 +
12 0 0 0 0 0 -


Test 1:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1   219.9974  +  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        350  0  Nachdruck 1 - Druck [bar]
 3   3947      0        240  0  Schmelzetemperatur [�C]
 4   3944      0         40  0  Wandtemperatur [�C]
 5   1030      0        9.5  0  Nachdruckzeit [s]
 6   3620      0         24  0  K�hlzeit im Werkzeug ab F�llende [s]


Test 2:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1    79.9974  -  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        350  0  Nachdruck 1 - Druck [bar]
 3   3947      0        240  0  Schmelzetemperatur [�C]
 4   3944      0         40  0  Wandtemperatur [�C]
 5   1030      0        9.5  0  Nachdruckzeit [s]
 6   3620      0         24  0  K�hlzeit im Werkzeug ab F�llende [s]


Test 3:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1   149.9974  0  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        600  +  Nachdruck 1 - Druck [bar]
 3   3947      0        240  0  Schmelzetemperatur [�C]
 4   3944      0         40  0  Wandtemperatur [�C]
 5   1030      0        9.5  0  Nachdruckzeit [s]
 6   3620      0         24  0  K�hlzeit im Werkzeug ab F�llende [s]


Test 4:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1   149.9974  0  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        100  -  Nachdruck 1 - Druck [bar]
 3   3947      0        240  0  Schmelzetemperatur [�C]
 4   3944      0         40  0  Wandtemperatur [�C]
 5   1030      0        9.5  0  Nachdruckzeit [s]
 6   3620      0         24  0  K�hlzeit im Werkzeug ab F�llende [s]


Test 5:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1   149.9974  0  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        350  0  Nachdruck 1 - Druck [bar]
 3   3947      0        260  +  Schmelzetemperatur [�C]
 4   3944      0         40  0  Wandtemperatur [�C]
 5   1030      0        9.5  0  Nachdruckzeit [s]
 6   3620      0         24  0  K�hlzeit im Werkzeug ab F�llende [s]


Test 6:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1   149.9974  0  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        350  0  Nachdruck 1 - Druck [bar]
 3   3947      0        220  -  Schmelzetemperatur [�C]
 4   3944      0         40  0  Wandtemperatur [�C]
 5   1030      0        9.5  0  Nachdruckzeit [s]
 6   3620      0         24  0  K�hlzeit im Werkzeug ab F�llende [s]


Test 7:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1   149.9974  0  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        350  0  Nachdruck 1 - Druck [bar]
 3   3947      0        240  0  Schmelzetemperatur [�C]
 4   3944      0         60  +  Wandtemperatur [�C]
 5   1030      0        9.5  0  Nachdruckzeit [s]
 6   3620      0         24  0  K�hlzeit im Werkzeug ab F�llende [s]


Test 8:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1   149.9974  0  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        350  0  Nachdruck 1 - Druck [bar]
 3   3947      0        240  0  Schmelzetemperatur [�C]
 4   3944      0         20  -  Wandtemperatur [�C]
 5   1030      0        9.5  0  Nachdruckzeit [s]
 6   3620      0         24  0  K�hlzeit im Werkzeug ab F�llende [s]


Test 9:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1   149.9974  0  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        350  0  Nachdruck 1 - Druck [bar]
 3   3947      0        240  0  Schmelzetemperatur [�C]
 4   3944      0         40  0  Wandtemperatur [�C]
 5   1030      0         16  +  Nachdruckzeit [s]
 6   3620      0         24  0  K�hlzeit im Werkzeug ab F�llende [s]


Test 10:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1   149.9974  0  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        350  0  Nachdruck 1 - Druck [bar]
 3   3947      0        240  0  Schmelzetemperatur [�C]
 4   3944      0         40  0  Wandtemperatur [�C]
 5   1030      0          3  -  Nachdruckzeit [s]
 6   3620      0         24  0  K�hlzeit im Werkzeug ab F�llende [s]


Test 11:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1   149.9974  0  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        350  0  Nachdruck 1 - Druck [bar]
 3   3947      0        240  0  Schmelzetemperatur [�C]
 4   3944      0         40  0  Wandtemperatur [�C]
 5   1030      0        9.5  0  Nachdruckzeit [s]
 6   3620      0     31.989  +  K�hlzeit im Werkzeug ab F�llende [s]


Test 12:

Nr Kenner Nummer       Wert  V  Bezeichnung [Einheit]
 1   5288      1   149.9974  0  F�llung 1 - Volumenstrom [cm�/s]
 2   5291      1        350  0  Nachdruck 1 - Druck [bar]
 3   3947      0        240  0  Schmelzetemperatur [�C]
 4   3944      0         40  0  Wandtemperatur [�C]
 5   1030      0        9.5  0  Nachdruckzeit [s]
 6   3620      0     16.011  -  K�hlzeit im Werkzeug ab F�llende [s]
