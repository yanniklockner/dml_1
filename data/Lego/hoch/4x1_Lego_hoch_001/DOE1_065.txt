4x1 Lego 2xhoch

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       238.8455658               240               230               250 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       240.1098328               240               230               250 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       157.0835114                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0       4.492200375                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       94.57566071                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       378.0536499                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       123.8675308                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       3.462968826                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       12.93920898                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       4.170843124                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       40.63702393                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0      0.9706550837                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.4072358906                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0       1.022589207                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       118.8456396               120       119.4000015       120.5999985 Laenge # Laenge [mm]
  3320      2       29.24918017                30       29.85000038       30.14999962 Breite # Breite [mm]
  3321      3        87.9465392        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4     0.02639293671     0.02509307861      0.0225837715     0.02760238573 Rundheit # Rundheit [mm]
  3320      5       95.17710592       96.17700195       95.69599915       96.65799713 Hoehe # Hoehe [mm]
  3350      0            54.578                 0                 0                 0 Bauteilgewicht [g]