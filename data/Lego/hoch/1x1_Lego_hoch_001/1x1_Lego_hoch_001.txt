1

1x1 Lego



--------------------------------------------------------------------------------

Eingabedaten



Cadmould� 3D-F Solver Version 9.0.1.0


Werkzeug:

Name: "1x1 Lego 2xhoch"
Oberfl�che = 20796 mm�
Volumen = 20573 mm�
Schwerpunkt = -0.000 , -0.000 , 46.073 mm
Kavit�tsvolumen * Dichte bei Umgebungsbedingungen = 18.6 g


Teil 1:

Oberfl�che = 20796 mm�
Volumen = 20573 mm�
Schwerpunkt = -0.000 , -0.000 , 46.073 mm
Kavit�tsvolumen * Dichte bei Umgebungsbedingungen = 18.6 g


Optionen Simulation - Modell:

Elementkantenl�nge:
Absolut [mm]: 1.44213

0.010 < Wanddicke [mm] < +oo

Kugel
Virtueller Verteiler


Finite-Elemente-Netz:

Volumen aller Dreieckselemente = 20223 mm�
-----------------------------------------------
Elementbezogenes Volumen = 20222.7 mm�

Volumen aller Formteile = 20573 mm�
-----------------------------------------------
Echtes Volumen = 20573 mm�

Elementbezogenes Volumen / Echtes Volumen = 0.9829677354


Anguss-Positionen:

 1 bei 15.000 , 0.000 , 26.000 mm , Oberfl�chennormale 1.000 , 0.000 , 0.000
 2 bei -15.000 , 0.000 , 26.000 mm , Oberfl�chennormale -1.000 , 0.000 , 0.000



Material:

Name: "STAMYLAN P 112MN40"
Typ: "PP"
Hersteller: "SABIC EUROPE PLASTICS"

Carreau-Parameter:
P1 = 232.378 Pa�s
P2 = 0.011322 s
P3 = 0.648075
T0 = 240 �C
Ts = 24.16 �C
Fp = 0 K/bar

Ei = 0 Pa�s

Fa = 8.86

Thermische Daten:
W�rmeleitf�higkeit = 0.18 W/(mK)
Temperaturleitf�higkeit = 0.0887749 mm�/s
Flie�grenztemperatur = 160 �C

PVT-Parameter:
PS1 = 59042 bar cm�/g
PS2 = 1.2 bar cm�/(gK)
PS3 = 1248.1 bar
PS4 = 52366 bar
PF1 = 64928 bar cm�/g
PF2 = 0.54285 bar cm�/(gK)
PF3 = 887.33 bar
PF4 = 59588 bar
PF5 = 1.835e-010 cm�/g
PF6 = 0.11536 1/k
PF7 = 0.0010874 1/bar
PK1 = 174.28 �C
PK2 = 0.0076119 K/bar
Dichte (23 �C) = 0.905 g/cm�

Empfohlene Prozess-Parameter:
Schmelzetemperatur = 240 �C
Wandtemperatur = 40 �C
Entformungstemperatur = 90 �C



Mechanische Daten:

E-Modul E(T) = E0 + E1�T + E2�T� + E3�T�, [T] = �C
E0 = 2815.2 MPa
E1 = -47.158 MPa/K
E2 = 0.3104 MPa/K�
E3 = -0.00078 MPa/K�

Querkontraktionszahl N(T) = N0 + N1�T + N2�T� + N3�T�, [T] = �C
N0 = 0.35
N1 = 0 1/K
N2 = 0 1/K�
N3 = 0 1/K�



Prozess-Parameter:

F�llzeit = 0.204365 s
Druckgesteuertes F�llen: 100 %
Schmelzetemperatur = 226 �C
Wandtemperatur = 26 �C
Entformungstemperatur = 60 �C

W�rme�bergangskoeffizient Werkzeug (F�llung) : Wanddickenabh�ngig
W�rme�bergangskoeffizient Werkzeug (Nachdruck) = 1000 W/(m�K)
W�rme�bergangskoeffizient Werkzeug (Restk�hlzeit) = 1000 W/(m�K)


Volumenstrom/Druck-Eingabe:

Typ: Balkendiagramm
Schalten abh. von F�llstand [%]

F�llung
1. F�llstand [%]: 100.0	Volumenstrom [cm�/s]: 100.669


Nachdruck-Eingabe:

Nachdruck
1. Nachdruckzeit [s]: 4.900	Druck [bar]: 173.00

K�hlzeit im Werkzeug ab F�llende = 18.3 s


Schwindung + Verzug Analyse:
W�rme�bergangskoeffizient Umgebung = 8 W/(m�K)
Umgebungstemperatur = 20 �C
Anisotropie = 0 
Mehrschrittig: Ja
Unwarp berechnen: Nein