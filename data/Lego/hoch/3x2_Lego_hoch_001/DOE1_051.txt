3x2 Lego 2xhoch

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0        223.471756               226               216               236 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0        226.371933               226               216               236 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       174.3442993                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0                 0                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       284.0602722                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       424.2753906                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       277.7041016                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       2.404420853                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0        13.3583107                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       2.860031605                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       19.78614616                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0      0.4975171685                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0     0.09886919707                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0      0.6800371408                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       89.33463039                90       89.55000305       90.44999695 Laenge # Laenge [mm]
  3320      2       59.41413262                60       59.70000076       60.29999924 Breite # Breite [mm]
  3321      3       89.79084205        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4     0.03000545502      0.0263671875       0.023730468       0.029003907 Rundheit # Rundheit [mm]
  3320      5       95.49872059       96.17700195       95.69599915       96.65799713 Hoehe # Hoehe [mm]
  3350      0            72.852                 0                 0                 0 Bauteilgewicht [g]