3x1 Lego 2xhoch

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0          239.4431               240               230               250 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       240.4897614               240               230               250 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       132.6995544                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0       3.546042204                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       94.78065491                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       283.9546814                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       93.18660736                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       3.547624111                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       14.51769638                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0        6.67118454                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       39.76800919                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0       1.071379542                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.7538149357                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0       1.175812602                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       89.07798883                90       89.55000305       90.44999695 Laenge # Laenge [mm]
  3320      2       28.15397075                30       29.85000038       30.14999962 Breite # Breite [mm]
  3321      3       89.03201562        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4     0.02581882477     0.02517032623     0.02265329286      0.0276873596 Rundheit # Rundheit [mm]
  3320      5       94.93044127       96.17700195       95.69599915       96.65799713 Hoehe # Hoehe [mm]
  3350      0            41.238                 0                 0                 0 Bauteilgewicht [g]