8x2 Lego 2xhoch

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0       230.2301331               254               244               264 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       254.0490417               254               244               264 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       214.7823334                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0                 0                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       103.7520218                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       351.2025757                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       227.8222656                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       4.802532196                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       15.63860035                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0        5.54795742                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       35.54498291                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0       1.752230883                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.1842608601                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0       1.295333624                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       236.7915554               240       238.8000031       241.1999969 Laenge # Laenge [mm]
  3320      2       59.08256708                60       59.70000076       60.29999924 Breite # Breite [mm]
  3321      3       89.79576202        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4     0.05712127686      0.0572347641     0.05151128769     0.06295824051 Rundheit # Rundheit [mm]
  3320      5       94.88173074       96.17700195       95.69599915       96.65799713 Hoehe # Hoehe [mm]
  3350      0           185.247                 0                 0                 0 Bauteilgewicht [g]