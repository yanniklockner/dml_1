8x1 Lego 2xhoch

    id  index     current_value         set_value       lower_limit       upper_limit name_unit
Kenner  Index    Aktueller Wert          Sollwert       Untergrenze        Obergrenze Bezeichnung [Einheit]
  3300      0                 0                 0                -1                10 F�llprobleme # F�llprobleme [%]
  3301      0        250.699707               254               244               264 Min. FF-Temp. # Min. FF-Temp. [�C]
  3302      0       254.1373444               254               244               264 Max. FF-Temp. # Max. FF-Temp. [�C]
  3303      0       195.8501282                 0                 0              2000 Max. F�lldruck # Max. F�lldruck [bar]
  3304      0       5.357296467                 0                -1                10 Einfallstellen # Einfallstellen [%]
  3678      0       142.7402649                 0                 0             20000 Schlie�kraft X # Schlie�kraft X [kN]
  3679      0       1139.696167                 0                 0             20000 Schlie�kraft Y # Schlie�kraft Y [kN]
  3680      0       372.6827698                 0                 0             20000 Schlie�kraft Z # Schlie�kraft Z [kN]
  3305      0       2.440514565                 0                -1                20 Min. Volumenschwindung # Min. Volumenschwindung [%]
  3306      0       13.64094734                 0                -1                20 Max. Volumenschwindung # Max. Volumenschwindung [%]
  3307      0       3.739233017                 0                -1                20 Mitt. Volumenschwindung # Mitt. Volumenschwindung [%]
  3308      0                 0                 0                -1                 1 Restdruck bei Entformung # Restdruck bei Entformung [bar]
  3309      0       34.69966888                 1                 0              2000 Erforderliche K�hlzeit # Erforderliche K�hlzeit [s]
  6929      0       1.353376269                 0                -1                20 Maximale Deformation # Maximale Deformation [mm]
  6930      0      0.7072364092                 0                -1                20 Maximaler Verzug # Maximaler Verzug [mm]
  5163      0      0.8635679483                 1                 0                 5 Mittlere Schwindung # Mittlere Schwindung [%]
  3320      1       238.3172113               240       238.8000031       241.1999969 Laenge # Laenge [mm]
  3320      2       28.34342771                30       29.85000038       30.14999962 Breite # Breite [mm]
  3321      3       89.72555883        90.0000025       89.00000134       90.99999684 Winkel # Winkel [�]
  3312      4     0.04865455627     0.04807949066      0.0432715416     0.05288743973 Rundheit # Rundheit [mm]
  3320      5       95.28828598       96.17700195       95.69599915       96.65799713 Hoehe # Hoehe [mm]
  3350      0           104.256                 0                 0                 0 Bauteilgewicht [g]