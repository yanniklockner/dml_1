# Change path
import sys
sys.path.append('C:\\Users\\lockner\\Python_Projects\\DML_1\\scripts')
sys.path.append('C:\\Users\\lockner\\python3.7.5\\lib\\site-packages')
# Import necessary modules
from os.path import exists, join
from os import mkdir
from modules import classes as cl
from modules import fParsing as fP
from random import seed, shuffle
import json
from sklearn.preprocessing import MinMaxScaler
from math import ceil
from modules import functions as ff
from modules import fANN
import pickle

# Define target domain
TARGET_DOMAIN = ('4x2', '3fach')

# Define results directory and data path
RESULTS = '../results'
RESULTS_THIS = join(RESULTS, 'conventionalapproach_reduced')
RESULTS_THIS_2 = join(RESULTS_THIS, str(TARGET_DOMAIN))
DISTRIBUTION_DIRECTORY = join(RESULTS_THIS_2, '01_distribution')
DATA_PATH = '../data/Lego'

# Check is directory for results exist, if not, make it
if not exists(RESULTS):
    mkdir(RESULTS)
if not exists(RESULTS_THIS):
    mkdir(RESULTS_THIS)
if not exists(RESULTS_THIS_2):
    mkdir(RESULTS_THIS_2)
if not exists(DISTRIBUTION_DIRECTORY):
    mkdir(DISTRIBUTION_DIRECTORY)

#%%
# Define Parameters
PARAMETERS = cl.Parameters(10, 'RMSProp', 'elu', 'l2', 'he_normal', 1, 
                           7, 0.8, 0.2, 0.0, 'mean_squared_error', 16, 
                           ['mean_squared_error', 'mean_absolute_error'])

# Define which distribution (see below) shall be used for training
CURRENT_DISTRIBUTION = '1'

# Define hiw many chunks shall be created
CHUNK_AMOUNT = 20

# Define the dictionary containing predictions and evaluations
PRED_N_EVAL = {}
PRED_N_EVAL["Target domain: "+str(TARGET_DOMAIN)] = {}
PRED_N_EVAL["Target domain: "+str(TARGET_DOMAIN)]["Distribution(Seed) "+CURRENT_DISTRIBUTION] = {}

#%%
# Parse the data and load into dictionary
PARSED_DATA = {}
PARSED_DATA['X'], PARSED_DATA['Y'], PARSED_DATA['G'] = fP.parsing_common_geometry_information(TARGET_DOMAIN[0], TARGET_DOMAIN[1], DATA_PATH)

#%%
# Generate 10 different distributions by setting random.seed(); store the dictionary as JSON
DISTRIBUTION_DATA = {}
DISTRIBUTION_DATA['X'] = {}
DISTRIBUTION_DATA['Y'] = {}
for seeding in range(10):
    PARSED_DATA_COPY = PARSED_DATA.copy()
    seed(seeding)
    shuffle(PARSED_DATA_COPY['X'])
    DISTRIBUTION_DATA['X'][str(seeding)] = PARSED_DATA_COPY['X']
    seed(seeding)
    shuffle(PARSED_DATA_COPY['Y'])
    DISTRIBUTION_DATA['Y'][str(seeding)] = PARSED_DATA_COPY['Y']

#convert tuple keys to str
DISTRIBUTION_DATA_JSON = {}
for tb in DISTRIBUTION_DATA:
    DISTRIBUTION_DATA_JSON[str(tb)] = DISTRIBUTION_DATA[tb]
with open(join(DISTRIBUTION_DIRECTORY, 'distribution_dictionary.json'), 'w') as dump:
    json.dump(DISTRIBUTION_DATA_JSON, dump)

#%%
# Preprocess the data by separate scaling and load into dictionary
# Do not scale here as the test data might generate a different scaler as the sole training data
#PREPROCESSED_DATA = {}
#PREPROCESSED_DATA['X'] = {}
#PREPROCESSED_DATA['Y'] = {}
#for str_seed in DISTRIBUTION_DATA['X']:
#    X_SCALER = MinMaxScaler()
#    print ("fitted scaler")
#    PREPROCESSED_DATA['X'][str_seed] = X_SCALER.fit_transform(DISTRIBUTION_DATA['X'][str_seed])
#for str_seed in DISTRIBUTION_DATA['Y']:
#    Y_SCALER = MinMaxScaler()
#    PREPROCESSED_DATA['Y'][str_seed] = Y_SCALER.fit_transform(DISTRIBUTION_DATA['Y'][str_seed])
    
PREPROCESSED_DATA = DISTRIBUTION_DATA
    
#%%
# Slice the data into 20 chunks per distribution
DATA_CHUNKS = {}
DATA_CHUNKS['X'] = {}
DATA_CHUNKS['Y'] = {}
for str_seed in PREPROCESSED_DATA['X']:
    DIV = ceil(len(PREPROCESSED_DATA['X'][str_seed]) / CHUNK_AMOUNT)
    CHUNK_LIST = []
    TICKER = 0
    while TICKER < len(PREPROCESSED_DATA['X'][str_seed]):
        CHUNK_LIST.append(PREPROCESSED_DATA['X'][str_seed][TICKER:(TICKER+DIV)])
        TICKER += DIV
    DATA_CHUNKS['X'][str_seed] = CHUNK_LIST
for str_seed in PREPROCESSED_DATA['Y']:
    DIV = ceil(len(PREPROCESSED_DATA['Y'][str_seed]) / CHUNK_AMOUNT)
    CHUNK_LIST = []
    TICKER = 0
    while TICKER < len(PREPROCESSED_DATA['Y'][str_seed]):
        CHUNK_LIST.append(PREPROCESSED_DATA['Y'][str_seed][TICKER:(TICKER+DIV)])
        TICKER += DIV
    DATA_CHUNKS['Y'][str_seed] = CHUNK_LIST

#%%
# Build the training and test dictionary
TRAINING_TEST = {}
TRAINING_TEST['X'] = {}
TRAINING_TEST['Y'] = {}

for str_seed in DATA_CHUNKS['X']:
    TRAINING_TEST['X'][str_seed] = {}
    for split in range(1, 20):
        TRAINING_TEST['X'][str_seed][str(5*split)+'% TRAIN'] = {}
        TRAINING_LIST = []
        TEST_LIST = []
        for chunk in range(split):
            for element in DATA_CHUNKS['X'][str_seed][chunk]:
                TRAINING_LIST.append(element)
        for chunk in range(split, 20):
            for element in DATA_CHUNKS['X'][str_seed][chunk]:
                TEST_LIST.append(element)
        TRAINING_TEST['X'][str_seed][str(5*split)+'% TRAIN']['X_TRAIN'] = TRAINING_LIST
        TRAINING_TEST['X'][str_seed][str(5*split)+'% TRAIN']['X_TEST'] = TEST_LIST
for str_seed in DATA_CHUNKS['Y']:
    TRAINING_TEST['Y'][str_seed] = {}
    for split in range(1, 20):
        TRAINING_LIST = []
        TEST_LIST = []
        TRAINING_TEST['Y'][str_seed][str(5*split)+'% TRAIN'] = {}
        for chunk in range(split):
            for element in DATA_CHUNKS['Y'][str_seed][chunk]:
                TRAINING_LIST.append(element)
        for chunk in range(split, 20):
            for element in DATA_CHUNKS['Y'][str_seed][chunk]:
                TEST_LIST.append(element)
        TRAINING_TEST['Y'][str_seed][str(5*split)+'% TRAIN']['Y_TRAIN'] = TRAINING_LIST
        TRAINING_TEST['Y'][str_seed][str(5*split)+'% TRAIN']['Y_TEST'] = TEST_LIST

#%%
# Training and storage

STORAGE_1 = join(RESULTS_THIS_2, "Distribution(Seed) "+CURRENT_DISTRIBUTION)
if not exists(STORAGE_1):
    mkdir(STORAGE_1)

for testpoint in TRAINING_TEST['X'][CURRENT_DISTRIBUTION]: # 19 testpoints
    
    PRED_N_EVAL["Target domain: "+str(TARGET_DOMAIN)]["Distribution(Seed) "+CURRENT_DISTRIBUTION][testpoint] = {}
    
    STORAGE_2 = join(STORAGE_1, testpoint)
    if not exists(STORAGE_2):
        mkdir(STORAGE_2)
    
    # Instatiate the scalers here by fitting to the provided training data
    X_SCALER = MinMaxScaler()
    X_TRAIN_SCALED = X_SCALER.fit_transform(TRAINING_TEST['X'][CURRENT_DISTRIBUTION][testpoint]['X_TRAIN'])
    Y_SCALER = MinMaxScaler()
    Y_TRAIN_SCALED = Y_SCALER.fit_transform(TRAINING_TEST['Y'][CURRENT_DISTRIBUTION][testpoint]['Y_TRAIN'])
    
    X_TEST = TRAINING_TEST['X'][CURRENT_DISTRIBUTION][testpoint]['X_TEST']
    Y_TEST = TRAINING_TEST['Y'][CURRENT_DISTRIBUTION][testpoint]['Y_TEST']
    
    # Ten initializations
    for index in range(2):
        
        PRED_N_EVAL["Target domain: "+str(TARGET_DOMAIN)]["Distribution(Seed) "+CURRENT_DISTRIBUTION][testpoint]["Index "+str(index)] = {}
        
        STORAGE_3 = join(STORAGE_2, "Initialization "+str(index))
        if not exists(STORAGE_3):
            mkdir(STORAGE_3)
        
        # Train and return model
        model = fANN.ANN_conv_approach(X_TRAIN_SCALED, Y_TRAIN_SCALED, PARAMETERS, STORAGE_3)
        
        # Scale the test data with the X_SCALER, use 'transform' not 'fit_transform' as the fitting was done with the training data
        X_TEST_SCALED = X_SCALER.transform(X_TEST)
        
        # Use the model to make a prediction
        Y_TEST_SCALED_PREDICTED = fANN.Y_predict(model, X_TEST_SCALED)
        
        # Use the Y_SCALER instance to unscale the data
        Y_TEST_PREDICTED = Y_SCALER.inverse_transform(Y_TEST_SCALED_PREDICTED)
    
        # Store the predictions in a dictionary
        PRED_N_EVAL["Target domain: "+str(TARGET_DOMAIN)]["Distribution(Seed) "+CURRENT_DISTRIBUTION][testpoint]["Index "+str(index)]["predictions"] = Y_TEST_PREDICTED
        PRED_N_EVAL["Target domain: "+str(TARGET_DOMAIN)]["Distribution(Seed) "+CURRENT_DISTRIBUTION][testpoint]["Index "+str(index)]["true_values"] = Y_TEST

# Store the PRED_N_EVAl Dictionary
STORE_PREDNEVAL = join(STORAGE_1, '00_pred_n_eval')
if not exists(STORE_PREDNEVAL):
    mkdir(STORE_PREDNEVAL)
with open(join(STORE_PREDNEVAL, 'pred_n_eval.pickle'), 'wb') as dump:
    pickle.dump(PRED_N_EVAL, dump)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

