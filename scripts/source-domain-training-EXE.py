# Change path
import sys
sys.path.append('C:\\Users\\lockner\\Python_Projects\\DML_1\\')
# Import necessary modules
# from scripts.modules import fParsing as fP
from scripts.modules import functions as ff
from scripts.modules import classes as cl
from os import mkdir, path
from os.path import exists, join
from scripts.modules import fParsing as fP
import random
import json
from sklearn.preprocessing import MinMaxScaler
from scripts.modules import fANN

# Get the list containing each domain description as tuple(size, configuration)
TBL = ff.toyBrickList()
# Define which distribution (see below) shall be used for training
CURRENT_DISTRIBUTION = '9'
# Define results directory
RESULTS = '../results'
# Check is directory for results exist, if not, make it
if not exists(RESULTS):
    mkdir(RESULTS)
# Update RESULTS to source-domain training
RESULTS = join(RESULTS, 'sourcedomains')
# Check is directory for results exist, if not, make it
if not exists(RESULTS):
    mkdir(RESULTS)
# Update directory according to current distribution
RESULTS_THIS = join(RESULTS, 'SD(S) '+CURRENT_DISTRIBUTION) # Source Distribution (Seed)
# Check is directory for results exist, if not, make it
if not exists(RESULTS_THIS):
    mkdir(RESULTS_THIS)
# Make a directory for every TOY_BRICK
for tb in TBL:
    if not exists(join(RESULTS_THIS, tb[0]+'_'+tb[1])):
        mkdir(join(RESULTS_THIS, tb[0]+'_'+tb[1]))
#Make a directory for the distribution dictionary
DISTRIBUTION_DIRECTORY = join(RESULTS, CURRENT_DISTRIBUTION+'_distribution')
if not exists(DISTRIBUTION_DIRECTORY):
    mkdir(DISTRIBUTION_DIRECTORY)

#%%
# Define Parameters
PARAMETERS = cl.Parameters(10, 'RMSProp', 'elu', 'l2', 'he_normal', 1, 
                           7, 0.8, 0.2, 0.0, 'mean_squared_error', 16, 
                           ['mean_squared_error', 'mean_absolute_error'])

#%%
# Parse the data and load into dictionary
PARSED_DATA = {}
# Define the data path
DATA_PATH = '..\data\Lego'
# Do the parsing for every combination in TOY_BRICK_LIST
for tb in TBL:
    PARSED_DATA[tb] = {}
    PARSED_DATA[tb]['X'], PARSED_DATA[tb]['Y'], PARSED_DATA[tb]['G'] = fP.parsing_common_geometry_information(tb[0], tb[1], DATA_PATH)
    
#%%
# Generate 10 different distributions by setting random.seed(); store the dictionary as JSON
DISTRIBUTION_DATA = {}
for tb in PARSED_DATA:
    DISTRIBUTION_DATA[tb] = {}
    DISTRIBUTION_DATA[tb]['X'] = {}
    DISTRIBUTION_DATA[tb]['Y'] = {}
    for seeding in range(10):
            
        TEMP_X = PARSED_DATA[tb]['X'].copy()
        random.seed(seeding)
        random.shuffle(TEMP_X)
        DISTRIBUTION_DATA[tb]['X'][str(seeding)] = TEMP_X
        
        TEMP_Y = PARSED_DATA[tb]['Y'].copy()
        random.seed(seeding)
        random.shuffle(TEMP_Y)
        DISTRIBUTION_DATA[tb]['Y'][str(seeding)] = TEMP_Y

#convert tuple keys to str
DISTRIBUTION_DATA_JSON = {}
for tb in DISTRIBUTION_DATA:
    DISTRIBUTION_DATA_JSON[str(tb)] = DISTRIBUTION_DATA[tb]
with open(join(DISTRIBUTION_DIRECTORY, 'distribution_dictionary.json'), 'w') as dump:
    json.dump(DISTRIBUTION_DATA_JSON, dump)
        
#%%
# Preprocess the data by separate scaling and load into dictionary
PREPROCESSED_DATA = {}
#Scaling
for tb in DISTRIBUTION_DATA:
    PREPROCESSED_DATA[tb] = {}
    PREPROCESSED_DATA[tb]['X'] = {}
    PREPROCESSED_DATA[tb]['Y'] = {}
    for str_seed in DISTRIBUTION_DATA[tb]['X']:
        X_SCALER = MinMaxScaler()
        PREPROCESSED_DATA[tb]['X'][str_seed] = X_SCALER.fit_transform(DISTRIBUTION_DATA[tb]['X'][str_seed])
    for str_seed in DISTRIBUTION_DATA[tb]['Y']:
        Y_SCALER = MinMaxScaler()
        PREPROCESSED_DATA[tb]['Y'][str_seed] = Y_SCALER.fit_transform(DISTRIBUTION_DATA[tb]['Y'][str_seed])
    
#%%
# Create a loop over 1) the domains and 2) stays in the domain for 10 repetitions and get the correct directory; use first distribution
for tb in PARSED_DATA:
    FILE_PATH = join(RESULTS_THIS, tb[0]+'_'+tb[1])
    for index in range(10):
        
#%%
# Create the artificial neural network, perform the training and store away the model and the training history
        fANN.ANN(PREPROCESSED_DATA[tb]['X'][CURRENT_DISTRIBUTION], PREPROCESSED_DATA[tb]['Y'][CURRENT_DISTRIBUTION], PARAMETERS, index, FILE_PATH, CURRENT_DISTRIBUTION)
        

        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
