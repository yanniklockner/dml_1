# Import all necessary files
from scripts.modules import fParsing as fP
from scripts.modules import functions as ff
from scripts.modules import classes as cl
from os import mkdir
from os.path import join, exists
import json
from math import ceil
from sklearn.preprocessing import MinMaxScaler
from keras.models import load_model
from scripts.modules import fANN
import pickle
import keras.backend as K

### Be aware of the chosen experimental design and choose your structure accordingly! ###

# Get all Source Domains
TBL = ff.toyBrickList()
# Define target domain
TARGET_DOMAIN = ('4x2', 'original')
# Define results directory and data path
RESULTS = 'A:/Zwischenresultate'
RESULTS_THIS = join(RESULTS, 'TL_reduced')
RESULTS_THIS_2 = join(RESULTS_THIS, 'TLTD '+str(TARGET_DOMAIN))
DISTRIBUTION_DIRECTORY = join(RESULTS_THIS_2, '01_distribution')
DATA_PATH = '../data/Lego'
# Check is directory for results exist, if not, make it
if not exists(RESULTS):
    mkdir(RESULTS)
if not exists(RESULTS_THIS):
    mkdir(RESULTS_THIS)
if not exists(RESULTS_THIS_2):
    mkdir(RESULTS_THIS_2)
if not exists(DISTRIBUTION_DIRECTORY):
    mkdir(DISTRIBUTION_DIRECTORY)

# %%
# Define Parameters
PARAMETERS = cl.Parameters(1, 'RMSProp', 'elu', 'l2', 'he_normal', 1,
                           7, 0.8, 0.2, 0.0, 'mean_squared_error', 16,
                           ['mean_squared_error', 'mean_absolute_error'])
# Define which distribution (see below) shall be used for training
CURRENT_DISTRIBUTION = '0'
# Define hiw many chunks shall be created
CHUNK_AMOUNT = 20

# %%
# Parse the data and load into dictionary
PARSED_DATA = {}
PARSED_DATA['X'], PARSED_DATA['Y'], PARSED_DATA['G'] = fP.parsing_common_geometry_information(TARGET_DOMAIN[0], TARGET_DOMAIN[1], DATA_PATH)

# %%
# Load the distribution data for for taget domain that was saved as a JSON file during the conventional approach training
if int(CURRENT_DISTRIBUTION) < 10:
    TEMP = '0'+CURRENT_DISTRIBUTION
else:
    TEMP = CURRENT_DISTRIBUTION
JSON_PATH = join(RESULTS, 'conventionalapproach_reduced', str(TARGET_DOMAIN), TEMP+'_distribution', 'distribution_dictionary.json')
with open(JSON_PATH, 'rb') as data_file:
    DISTRIBUTION_DATA = json.load(data_file)

# %%
# Preprocess the data by separate scaling and load into dictionary
# Do not scale here as the test data might generate a different scaler as the sole training data
PREPROCESSED_DATA = DISTRIBUTION_DATA

# %%
# Slice the data into 20 chunks per distribution
DATA_CHUNKS = {}
DATA_CHUNKS['X'] = {}
DATA_CHUNKS['Y'] = {}
for str_seed in PREPROCESSED_DATA['X']:
    DIV = ceil(len(PREPROCESSED_DATA['X'][str_seed]) / CHUNK_AMOUNT)
    CHUNK_LIST = []
    TICKER = 0
    while TICKER < len(PREPROCESSED_DATA['X'][str_seed]):
        CHUNK_LIST.append(PREPROCESSED_DATA['X'][str_seed][TICKER:(TICKER+DIV)])
        TICKER += DIV
    DATA_CHUNKS['X'][str_seed] = CHUNK_LIST
for str_seed in PREPROCESSED_DATA['Y']:
    DIV = ceil(len(PREPROCESSED_DATA['Y'][str_seed]) / CHUNK_AMOUNT)
    CHUNK_LIST = []
    TICKER = 0
    while TICKER < len(PREPROCESSED_DATA['Y'][str_seed]):
        CHUNK_LIST.append(PREPROCESSED_DATA['Y'][str_seed][TICKER:(TICKER+DIV)])
        TICKER += DIV
    DATA_CHUNKS['Y'][str_seed] = CHUNK_LIST

# %%
# Build the training and test dictionary
TRAINING_TEST = {}
TRAINING_TEST['X'] = {}
TRAINING_TEST['Y'] = {}

for str_seed in DATA_CHUNKS['X']:
    TRAINING_TEST['X'][str_seed] = {}
    for split in range(1, 20):
        TRAINING_TEST['X'][str_seed][str(5*split)+'% TRAIN'] = {}
        TRAINING_LIST = []
        TEST_LIST = []
        for chunk in range(split):
            for element in DATA_CHUNKS['X'][str_seed][chunk]:
                TRAINING_LIST.append(element)
        for chunk in range(split, 20):
            for element in DATA_CHUNKS['X'][str_seed][chunk]:
                TEST_LIST.append(element)
        TRAINING_TEST['X'][str_seed][str(5*split)+'% TRAIN']['X_TRAIN'] = TRAINING_LIST
        TRAINING_TEST['X'][str_seed][str(5*split)+'% TRAIN']['X_TEST'] = TEST_LIST
for str_seed in DATA_CHUNKS['Y']:
    TRAINING_TEST['Y'][str_seed] = {}
    for split in range(1, 20):
        TRAINING_LIST = []
        TEST_LIST = []
        TRAINING_TEST['Y'][str_seed][str(5*split)+'% TRAIN'] = {}
        for chunk in range(split):
            for element in DATA_CHUNKS['Y'][str_seed][chunk]:
                TRAINING_LIST.append(element)
        for chunk in range(split, 20):
            for element in DATA_CHUNKS['Y'][str_seed][chunk]:
                TEST_LIST.append(element)
        TRAINING_TEST['Y'][str_seed][str(5*split)+'% TRAIN']['Y_TRAIN'] = TRAINING_LIST
        TRAINING_TEST['Y'][str_seed][str(5*split)+'% TRAIN']['Y_TEST'] = TEST_LIST

# %%
# Training and storage
# Target domain and distribution are fixed
# Set source path
SOURCE_PATH = join(RESULTS, 'sourcedomains')
# Creating directory 1
STORAGE_1 = join(RESULTS_THIS_2, "TLD(S) "+CURRENT_DISTRIBUTION)  # TLDS(S) == Transfer Learning Distribution (Seed)
if not exists(STORAGE_1):
    mkdir(STORAGE_1)
# Looping over testpoints
for testpoint in TRAINING_TEST['X'][CURRENT_DISTRIBUTION]:  # 19 testpoints, i.e. "5% TRAIN" or "70% TRAIN"
    # Possible splitting of computational load #######################
    if testpoint == "5% TRAIN":
    # Possible splitting of computational load #######################
        # Creating directory 2
        STORAGE_2 = join(STORAGE_1, 'TLTP '+testpoint)  # TLTP == Transfer Learning TestPoint
        if not exists(STORAGE_2):
            mkdir(STORAGE_2)
        # Instantiate the scalers here by fitting to the provided training data
        X_SCALER = MinMaxScaler()
        X_TRAIN_SCALED = X_SCALER.fit_transform(TRAINING_TEST['X'][CURRENT_DISTRIBUTION][testpoint]['X_TRAIN'])
        Y_SCALER = MinMaxScaler()
        Y_TRAIN_SCALED = Y_SCALER.fit_transform(TRAINING_TEST['Y'][CURRENT_DISTRIBUTION][testpoint]['Y_TRAIN'])
        # Set the test datasets
        X_TEST = TRAINING_TEST['X'][CURRENT_DISTRIBUTION][testpoint]['X_TEST']
        Y_TEST = TRAINING_TEST['Y'][CURRENT_DISTRIBUTION][testpoint]['Y_TEST']
        # Scale the test data with the X_SCALER, use 'transform' not 'fit_transform' as the fitting was done with the training data
        X_TEST_SCALED = X_SCALER.transform(X_TEST)
        # Looping over source domains except the target domain
        sourcedomains = {1: '1x1', 2: '1x2', 3: '2x1', 4: '2x2', 5: '3x1', 6: '3x2', 7: '4x1', 8: '4x2', 
                          9: '6x1', 10: '6x2', 11: '8x1', 12: '8x2'}
        for domain in TBL:  # Source Model Domain
            for n in range (1, 13):
            # Possible splitting of computational load ####################### str('dimension_' + str(n))
                if not domain == TARGET_DOMAIN and domain == (sourcedomains[n], '3fach'):
                    print("Checkpoint 1")
                # Possible splitting of computational load #######################
                    # Creating directory 3
                    STORAGE_3 = join(STORAGE_2, 'SMD '+str(domain))  # SMD == Source Model Domain
                    if not exists(STORAGE_3):
                        mkdir(STORAGE_3)
                    # Creating directory 3.1
                    STORE_PRED_N_EVAL = join(STORAGE_3, 'P_N_E SM(D) '+str(domain))
                    if not exists(STORE_PRED_N_EVAL):
                        mkdir(STORE_PRED_N_EVAL)
                    # Initialize dictionary
                    PRED_N_EVAL = {}
                    PRED_N_EVAL["TLTD "+str(TARGET_DOMAIN)] = {}
                    PRED_N_EVAL["TLTD "+str(TARGET_DOMAIN)]["TLD(S) "+CURRENT_DISTRIBUTION] = {}
                    PRED_N_EVAL["TLTD "+str(TARGET_DOMAIN)]["TLD(S) "+CURRENT_DISTRIBUTION]['TLTP '+testpoint] = {}
                    PRED_N_EVAL["TLTD "+str(TARGET_DOMAIN)]["TLD(S) "+CURRENT_DISTRIBUTION]['TLTP '+testpoint]['SMD '+str(domain)] = {}
                    print("Checkpoint 2")
                    # Looping over distributions of source domains
                    for source_distribution in range(10):  # Source Distribution(Seed) ; The distribution with which the source model was trained
                        # Initialize the result dictionary for storage
                        PRED_N_EVAL["TLTD "+str(TARGET_DOMAIN)]["TLD(S) "+CURRENT_DISTRIBUTION]['TLTP '+testpoint]['SMD '+str(domain)]['SD(S) '+str(source_distribution)] = {}
                        # Creating directory 4
                        STORAGE_4 = join(STORAGE_3, 'SD(S) '+str(source_distribution))  # SD(S) = Source Domain (Seed)
                        if not exists(STORAGE_4):
                            mkdir(STORAGE_4)
                        # Looping over initilization of source models
                        for source_initialization in range(10):  # Source Init  ialization for a certain distribution of a source domain
                            PRED_N_EVAL["TLTD "+str(TARGET_DOMAIN)]["TLD(S) "+CURRENT_DISTRIBUTION]['TLTP '+testpoint]['SMD '+str(domain)]['SD(S) '+str(source_distribution)]['SI '+str(source_initialization)] = {}
                            # Creating directory 5
                            STORAGE_5 = join(STORAGE_4, 'SI '+str(source_initialization))  # SI == Source Initilization
                            if not exists(STORAGE_5):
                                mkdir(STORAGE_5)
                            # Load the correct source model (1/2)
                            TEMP = str(domain).split("'")
                            MODEL_PATH = join(SOURCE_PATH, 'SD(S) '+str(source_distribution), TEMP[1]+'_'+TEMP[3], str(source_distribution)+'_'+str(source_initialization)+'.hdf5')
                            # Looping over chosen number of initilizations for TL
                            for TL_initialization in range(10):  # Each training 10 times to rule out random effects
                                PRED_N_EVAL["TLTD "+str(TARGET_DOMAIN)]["TLD(S) "+CURRENT_DISTRIBUTION]['TLTP '+testpoint]['SMD '+str(domain)]['SD(S) '+str(source_distribution)]['SI '+str(source_initialization)]['Initialization_TL_'+str(TL_initialization)] = {}
                                # Creating directory 6
                                STORAGE_6 = join(STORAGE_5, 'Initialization_TL_ '+str(TL_initialization))
                                if not exists(STORAGE_6):
                                    mkdir(STORAGE_6)
                                # Load the correct source model (2/2)
                                # Model needs to be reloaded every time after the session is cleared
                                # Because loaded model and the belonging graph is also cleared and must therefore be reconstructed
                                SOURCE_MODEL = load_model(MODEL_PATH)
    
                                # Complete Model (CM) is transferred
                                # Train and return model
                                model = fANN.ANN_TL_reduced_CM(X_TRAIN_SCALED, Y_TRAIN_SCALED, PARAMETERS, STORAGE_6, SOURCE_MODEL)
                                # Use the model to make a prediction
                                Y_TEST_SCALED_PREDICTED = fANN.Y_predict(model, X_TEST_SCALED)
                                # Use the Y_SCALER instance to unscale the data
                                Y_TEST_PREDICTED = Y_SCALER.inverse_transform(Y_TEST_SCALED_PREDICTED)
                                # Store the results
                                PRED_N_EVAL["TLTD "+str(TARGET_DOMAIN)]["TLD(S) "+CURRENT_DISTRIBUTION]['TLTP '+testpoint]['SMD '+str(domain)]['SD(S) '+str(source_distribution)]['SI '+str(source_initialization)]['Initialization_TL_'+str(TL_initialization)]['CM_predictions'] = Y_TEST_PREDICTED
                                PRED_N_EVAL["TLTD "+str(TARGET_DOMAIN)]["TLD(S) "+CURRENT_DISTRIBUTION]['TLTP '+testpoint]['SMD '+str(domain)]['SD(S) '+str(source_distribution)]['SI '+str(source_initialization)]['Initialization_TL_'+str(TL_initialization)]['CM_true_values'] = Y_TEST
    
                                # Input and hidden layer (IHL) are transferred
                                # Train and return model
                                model = fANN.ANN_TL_reduced_IHL(X_TRAIN_SCALED, Y_TRAIN_SCALED, PARAMETERS, STORAGE_6, SOURCE_MODEL)
                                # Use the model to make a prediction
                                Y_TEST_SCALED_PREDICTED = fANN.Y_predict(model, X_TEST_SCALED)
                                # Use the Y_SCALER instance to unscale the data
                                Y_TEST_PREDICTED = Y_SCALER.inverse_transform(Y_TEST_SCALED_PREDICTED)
                                # Store the results
                                PRED_N_EVAL["TLTD "+str(TARGET_DOMAIN)]["TLD(S) "+CURRENT_DISTRIBUTION]['TLTP '+testpoint]['SMD '+str(domain)]['SD(S) '+str(source_distribution)]['SI '+str(source_initialization)]['Initialization_TL_'+str(TL_initialization)]['IHL_predictions'] = Y_TEST_PREDICTED
                                PRED_N_EVAL["TLTD "+str(TARGET_DOMAIN)]["TLD(S) "+CURRENT_DISTRIBUTION]['TLTP '+testpoint]['SMD '+str(domain)]['SD(S) '+str(source_distribution)]['SI '+str(source_initialization)]['Initialization_TL_'+str(TL_initialization)]['IHL_true_values'] = Y_TEST
    
                                # Input Layer (IL) is transferred
                                # Train and return model
                                model = fANN.ANN_TL_reduced_IL(X_TRAIN_SCALED, Y_TRAIN_SCALED, PARAMETERS, STORAGE_6, SOURCE_MODEL)
                                # Use the model to make a prediction
                                Y_TEST_SCALED_PREDICTED = fANN.Y_predict(model, X_TEST_SCALED)
                                # Use the Y_SCALER instance to unscale the data
                                Y_TEST_PREDICTED = Y_SCALER.inverse_transform(Y_TEST_SCALED_PREDICTED)
                                # Store the results
                                PRED_N_EVAL["TLTD "+str(TARGET_DOMAIN)]["TLD(S) "+CURRENT_DISTRIBUTION]['TLTP '+testpoint]['SMD '+str(domain)]['SD(S) '+str(source_distribution)]['SI '+str(source_initialization)]['Initialization_TL_'+str(TL_initialization)]['IL_HL_predictions'] = Y_TEST_PREDICTED
                                PRED_N_EVAL["TLTD "+str(TARGET_DOMAIN)]["TLD(S) "+CURRENT_DISTRIBUTION]['TLTP '+testpoint]['SMD '+str(domain)]['SD(S) '+str(source_distribution)]['SI '+str(source_initialization)]['Initialization_TL_'+str(TL_initialization)]['IL_HL_true_values'] = Y_TEST
    
                                # Clear the tf nodes
                                K.clear_session()
                                
                    n += 1
                    # Store the pred_n_eval dictionary
                    with open(join(STORE_PRED_N_EVAL, 'pred_n_eval_partial.pickle'), 'wb') as dump:
                        pickle.dump(PRED_N_EVAL, dump) 

    
                  
            
