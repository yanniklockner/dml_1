# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 11:31:11 2019

@author: lockner
"""

def grid_search_dictionary():
    
    #Import external modules
    import tensorflow.keras.activations
    import tensorflow.keras.optimizers
    import tensorflow.initializers
    
    #getting external wrappers for hyperparameters
    #Annotation: Activation function will be defined as activation-argument (string) in the layer. For other options see tensorflow or keras documentation
    #Annotation: Initialization will be defined as initialization-argument (string) in the layer. For other options see tensorflow or keras documentation
    #Annotation: Optimizers will be reffered to as strings, problems with integration of tensorflow-onjects in keras-sequential
    #Annotation: Regularizers will be reffered to as strings as tf objects are not json-serializable
    ##EXAMPLE
    #RMSprop = tensorflow.keras.optimizers.RMSprop()
    #ADAM = tensorflow.keras.optimizers.Adam()
    #NADAM = tensorflow.keras.optimizers.Nadam()
    #L1 = tensorflow.keras.regularizers.l1(l=0.01)
    #L2 = tensorflow.keras.regularizers.l2(l=0.01)
    #L1L2 = tensorflow.keras.regularizers.l1_l2(l1=0.9, l2=0.1)
    
    ##TODO##
    #Keras regularizers does not recognize the l1_l2 (Elastic Net) class which should actually be built in
        
    #setting default hyperparameter grid search conditions
    ##EXAMPLE
    GS = {}
    GS['activation'] = ['elu', 'relu', 'tanh']
    GS['optimizer'] = ['rmsprop', 'adam', 'nadam']
    GS['regularizer'] = ['l2', 'l1', 'l1_l2']
    GS['initializer'] = ['he_normal', 'glorot_normal', 'random_normal']
    GS['maximaltrainingcycles'] = [50, 100, 150]
    GS['hiddenlayers'] = [1,2,3]
    GS['hiddenlayer1neurons'] = [13,14,15]
    GS['hiddenlayer2neurons'] = [10,11,12]
    GS['hiddenlayer3neurons'] = [7,8,9]

    #Return the dictionary
    return GS


def random_combinations(GS, hp_combinations):
    
    #Import external modules
    import random
    
    #Dictionary with resulting combinations
    #Dictionary structure is: level 1 keys are counting number of hyperparameter-combination, level 2 is name of hyperparameter, value is value of hyperparameter
    combinations_dict = {}
    
    #Generate the defined amount of combinations
    for i in range(hp_combinations):
        #Initialize a dictionary for the randomly selected hyperparameters
        combinations_dict[i] = {}
        #Select a hyperparameter from GS
        for key in GS:
            selection = random.choice(GS[key])
            #Create entry in dict
            combinations_dict[i][key] = selection
            
    ##TEST##check if all lists are complete
    for i in combinations_dict:
        if len(combinations_dict[i]) != len(GS):
            print ('Für die Hyperparameter-Kombination ' + str(i) + ' wurden anstatt ' + str(len(GS)) + ' nur ' + str(len(combinations_dict[i])) + 
                   ' Parameter ausgewählt. Hier ist etwas schief gelaufen.')
    
    ##TEST##check if the necessary amount of hyperparameter-combinations has been generated and stored in the dictionary
    if len(combinations_dict) != hp_combinations:
        print ('Es wurden nur ' + str(len(combinations_dict)) + ' Hyperparameter-Kombinationen erzeugt und ausgegeben anstatt der gewünschten Anzahl von ' + 
               str(hp_combinations) + '. Hier ist etwas schief gelaufen.')#
        
    #Return the dictionary with the hyperparameter-combinations
    return combinations_dict


def default():
    
    #Import external modules
    import tensorflow.keras.optimizers
    from tensorflow.keras.regularizers import l2
    import tensorflow.initializers
    
    #getting external wrappers for hyperparameters
    #Annotation: Activation function will be defined as activation-argument (string) in the layer. For other options see tensorflow or keras documentation
    #Annotation: Initialization will be defined as initialization-argument (string) in the layer. For other options see tensorflow or keras documentation
    #Annotation: Optimizers will be reffered to as strings, problems with integration of tensorflow-onjects in keras-sequential
    #Annotation: Regularizers will be reffered to as strings as tf objects are not json-serializable
    ##EXAMPLE
    #RMSprop = tensorflow.keras.optimizers.RMSprop()
    #L1 = tensorflow.keras.regularizers.l1(l=0.01)
           
    #setting default value for hyperparameters
    ##EXAMPLE
    HP = {}
    HP['activation'] = 'elu'
    HP['optimizer'] = 'rmsprop'
    HP['regularizer'] = l2(0.01)
    HP['initializer'] = 'he_normal'
    HP['maximaltrainingcycles'] = 2000
    HP['hiddenlayers'] = 1
    HP['hiddenlayer1neurons'] = 6

    #Return the dictionary with the fixed hyperparameters
    return HP


#Generates the np-arrays from indices generated in the stratified k_fold function and returns the np-arrays
def generateArrays(X_Train, X_Test, Y_Train, Y_Test):
    
    #Import necessary external modules
    import numpy as np
    
    #Transform the lists into arrays
    #Annotation: As len(X_Train) will never be 1, no dimensional adjustments need to be done
    X_Train = np.array(X_Train)
    Y_Train = np.array(Y_Train)
    
    #Transform into correct dimensions for training
    if len(X_Test) == 1:
        X_Test = np.array([X_Test])
    else:
        X_Test = np.array(X_Test)

    if len(Y_Test) == 1:
        Y_Test = np.array([Y_Test])
    else:
        Y_Test = np.array(Y_Test)
    
    #return the initialized lists
    return X_Train, X_Test, Y_Train, Y_Test
        