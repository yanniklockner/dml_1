# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 17:06:30 2018

@author: Yannik Lockner
"""

from sklearn.preprocessing import MinMaxScaler 

def scaling(XInput, YInput):
    
    scaler = MinMaxScaler(feature_range=(-1, 1))
    X = scaler.fit_transform(XInput)
    Y = scaler.fit_transform(YInput)
    
    return X, Y, scaler

def unscaling(Y_pred_scaled, Y_test, scaler):
    
    Y_pred_unscaled = scaler.inverse_transform(Y_pred_scaled)
    Y_test_unscaled = scaler.inverse_transform(Y_test)
    
    return Y_pred_unscaled, Y_test_unscaled
