# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 18:09:14 2018

@author: Yannik Lockner
"""
import math
from matplotlib import pyplot
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import r2_score

def analysis(Y_pred_unscaled, Y_test_unscaled, activation):

    mse_score = mean_squared_error(Y_test_unscaled, Y_pred_unscaled)
    rmse_score = math.sqrt(mse_score)
    mae_score = mean_absolute_error(Y_test_unscaled, Y_pred_unscaled)
    R2_score = r2_score(Y_test_unscaled, Y_pred_unscaled)
    
    print('MSE is: ', mse_score)
    print('RMSE is: ', rmse_score)
    print('MAE is: ', mae_score)
    print('R2 is: ', R2_score)
    
    return mse_score, rmse_score, mae_score, R2_score

def trainingAnalysis(trainingHistory):

    mse_score = trainingHistory.history["val_mean_squared_error"][-1]
    rmse_score = math.sqrt(mse_score)
    #mae_score = mean_absolute_error(Y_test_unscaled, Y_pred_unscaled)
    #R2_score = r2_score(Y_test_unscaled, Y_pred_unscaled)
    
    print('MSE(skaliert) der Validierungsdaten ist: ', mse_score)
    print('RMSE(skaliert) der Validierungsdaten ist: ', rmse_score)
    #print('MAE is: ', mae_score)
    #print('R2 is: ', R2_score)
    
    return rmse_score

def lossPlotter(trainingHistory, fold, lengthKFoldDict, element, combination, lengthHyperComDict, count, numberNets):
    
    #control row
    print("\nErgebnisse für Train/Val/Test Aufteilung " + str(fold+1) + " / " + str(lengthKFoldDict) + ", innere CV Aufteilung " + str(element+1) + " / " + str(lengthKFoldDict) + ", HPK " + str(combination+1) + " / " + str(lengthHyperComDict) + " und Netz " + str(count+1) + " / " + str(numberNets) + " :")

    #Create Plot  
    train_history_rmse = []
    val_history_rmse = []
    #modell.load_weights(filepath) 
    train_history= trainingHistory.history["loss"]
    # Recalculation so that it shows RMSE
    for i in train_history:
        train_history_rmse.append(math.sqrt(i))
    val_history = trainingHistory.history["val_loss"]
    for i in val_history:
        val_history_rmse.append(math.sqrt(i))
    pyplot.plot(train_history_rmse, color='blue', label='training loss')
    pyplot.plot(val_history_rmse, color='orange', label='validation loss')
    pyplot.title('Training loss and Validation loss')
    pyplot.ylabel('loss [rmse]')
    pyplot.xlabel('epoch [-]')
    pyplot.legend()
    pyplot.show()
    
    
def lossPlotterX(trainingHistory):
    
    #Create Plot  
    train_history_rmse = []
    val_history_rmse = []
    #modell.load_weights(filepath) 
    train_history= trainingHistory.history["loss"]
    # Recalculation so that it shows RMSE
    for i in train_history:
        train_history_rmse.append(math.sqrt(i))
    val_history = trainingHistory.history["val_loss"]
    for i in val_history:
        val_history_rmse.append(math.sqrt(i))
    pyplot.plot(train_history_rmse, color='blue', label='training loss')
    pyplot.plot(val_history_rmse, color='orange', label='validation loss')
    pyplot.title('Training loss and Validation loss')
    pyplot.ylabel('loss [rmse]')
    pyplot.xlabel('epoch [-]')
    pyplot.legend()
    pyplot.show()
    
def lossPlotterEasy(trainingHistory):
    
    #Create Plot  
    train_history_rmse = []
    train_history= trainingHistory.history["loss"]
    
    # Recalculation so that it shows RMSE
    for i in train_history:
        train_history_rmse.append(math.sqrt(i))
    pyplot.plot(train_history_rmse, color='blue', label='training loss')
    pyplot.title('Training loss')
    pyplot.ylabel('loss [rmse]')
    pyplot.xlabel('epoch [-]')
    pyplot.legend()
    pyplot.show()

    
def correlation(temp_dict_result):
    
    import pandas as pd
    import matplotlib.pyplot as plt
    
    test_dict = {}
    
    #transcribe into dict-order, compatibel with pandas dataframe         
    test_dict["Test"] = []
    test_dict["Füllung 1 - Volumenstrom [cm³/s]"] = []
    test_dict["Nachdruck 1 - Druck [bar]"] = []
    test_dict["Schmelzetemperatur [°C]"] = []
    test_dict["Heißkanaltemperatur [°C]"] = []
    test_dict["Nachdruckzeit [s]"] = []
    test_dict["Kühlzeit im Werkzeug ab Füllende [s]"] = []
    test_dict["Oberfläche [mm²]"] = []
    test_dict["Volumen [mm³]"] = []
    test_dict["Kompaktheit [-]"] = []
    schlüssel = list(temp_dict_result.keys())
    
    for element in temp_dict_result:
        test_dict["Test"].append(float(schlüssel.index(element)+1))
        test_dict["Füllung 1 - Volumenstrom [cm³/s]"].append(float(temp_dict_result[element][0]))
        test_dict["Nachdruck 1 - Druck [bar]"].append(float(temp_dict_result[element][1]))
        test_dict["Schmelzetemperatur [°C]"].append(float(temp_dict_result[element][2]))
        test_dict["Heißkanaltemperatur [°C]"].append(float(temp_dict_result[element][3]))
        test_dict["Nachdruckzeit [s]"].append(float(temp_dict_result[element][4]))
        test_dict["Kühlzeit im Werkzeug ab Füllende [s]"].append(float(temp_dict_result[element][5]))
        test_dict["Oberfläche [mm²]"].append(float(temp_dict_result[element][6]))
        test_dict["Volumen [mm³]"].append(float(temp_dict_result[element][7]))
        test_dict["Kompaktheit [-]"].append(float(temp_dict_result[element][8]))
    
    df = pd.DataFrame.from_dict(test_dict)
    
    #ceate correlation matrix
    
    df_corr = df.corr()
    
    #print correlation chart between 'Bauteilgewicht" and else
    
    plt.figure()
    df_corr.iloc[0].plot(kind="bar")
    plt.show()


#stdev() raises error when selecting numberNets == 1 b/c only one data point in list
def bestparams(results_dict, hyperComDict):
    
    import statistics as sc
    
    rmse_per_combination = []
    stdev_of_rmse_per_combination = []
    for combination in results_dict:
        rmse_per_fold = []
        for fold in results_dict[combination]:
            rmse_per_net = []
            for net in results_dict[combination][fold]:
                rmse_per_net.append(results_dict[combination][fold][net]["rmse"])
            rmse_per_fold.append(sc.mean(rmse_per_net))
        rmse_per_combination.append(sc.mean(rmse_per_fold))
        stdev_of_rmse_per_combination.append(sc.stdev(rmse_per_fold))
    
    show_dict = {}
    for combination in hyperComDict:
        stringx = str(hyperComDict[combination])
        rmse_value = rmse_per_combination[combination-1]
        stdev_value = stdev_of_rmse_per_combination[combination-1]
        show_dict[stringx] = [rmse_value, stdev_value]
        
    print (show_dict)