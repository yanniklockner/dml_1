# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 17:41:08 2019

@author: lockner
"""

# sourcedomain
def ANN(X, Y, PARAMETERS, index, FILE_PATH, CURRENT_DISTRIBUTION):
    
    #Import the necessary external modules
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import ModelCheckpoint, EarlyStopping
    from os.path import join
    import pickle
    import keras.backend as K
    
    #Create filepath for the model storage
    MODEL_FILE = join(FILE_PATH, CURRENT_DISTRIBUTION + '_' + str(index) + ".hdf5")
    TRAINING_HISTORY_FILE = join(FILE_PATH, CURRENT_DISTRIBUTION + '_' + str(index) + '_' + "hist.json")
    
    #Add the checkpoint for the call back list
    CALLBACKS = []
    CHECKPOINT = ModelCheckpoint(MODEL_FILE, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='min', period=1)
    EARLYSTOPPING = EarlyStopping(monitor='val_loss', min_delta=0, patience=100, verbose=1, mode='auto', baseline=None, restore_best_weights=True)
    CALLBACKS.append(CHECKPOINT)
    CALLBACKS.append(EARLYSTOPPING)
    
    #Initialize the ANN-instance with the Sequential() class from Keras
    model = Sequential()
    #Add input layer
    model.add(Dense(len(X[0]), activation=PARAMETERS.activation, kernel_regularizer=PARAMETERS.kernel_regularizer, 
                    kernel_initializer=PARAMETERS.kernel_initializer, name='input'))
    
    #Add Hidden Dense Layers
    model.add(Dense(PARAMETERS.hidden_neurons, activation=PARAMETERS.activation, kernel_regularizer=PARAMETERS.kernel_regularizer, 
                    kernel_initializer=PARAMETERS.kernel_initializer, name='hidden'))
    
    #Add output layer
    model.add(Dense(len(Y[0]), activation='linear', kernel_initializer=PARAMETERS.kernel_initializer, name='output'))
    
    #Compile the model
    model.compile(optimizer=PARAMETERS.optimizer, loss=PARAMETERS.loss, metrics=PARAMETERS.metrics, loss_weights=None, sample_weight_mode=None, 
                  weighted_metrics=None, target_tensors=None)
    
    #Fit the model to the data and store the history object
    training_history = model.fit(x=X, y=Y, batch_size=PARAMETERS.batch_size, epochs=PARAMETERS.max_cycles, verbose=1, 
                                 callbacks=CALLBACKS, validation_split=PARAMETERS.validation_percentage, validation_data=None, shuffle=False, 
                                 class_weight=None, sample_weight=None, initial_epoch=0, steps_per_epoch=None, validation_steps=None)
    
    #Store away the training history file
    with open(TRAINING_HISTORY_FILE, 'wb') as dump:
        pickle.dump(training_history, dump)

# conventionalapproach_reduced
def ANN_conv_approach(X, Y, PARAMETERS, FILE_PATH):
    
    #Import the necessary external modules
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import ModelCheckpoint, EarlyStopping
    from os.path import join
    import pickle
    import keras.backend as K
    
    #Create filepath for the model storage
    MODEL_FILE = join(FILE_PATH, "model.hdf5")
    TRAINING_HISTORY_FILE = join(FILE_PATH, "hist.json")
    
    #Add the checkpoint for the call back list
    CALLBACKS = []
    CHECKPOINT = ModelCheckpoint(MODEL_FILE, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='min', period=1)
    EARLYSTOPPING = EarlyStopping(monitor='val_loss', min_delta=0, patience=100, verbose=1, mode='auto', baseline=None, restore_best_weights=True)
    CALLBACKS.append(CHECKPOINT)
    CALLBACKS.append(EARLYSTOPPING)
    
    #Initialize the ANN-instance with the Sequential() class from Keras
    model = Sequential()
    #Add input layer
    model.add(Dense(len(X[0]), activation=PARAMETERS.activation, kernel_regularizer=PARAMETERS.kernel_regularizer, 
                    kernel_initializer=PARAMETERS.kernel_initializer, name='input'))
    
    #Add Hidden Dense Layers
    model.add(Dense(PARAMETERS.hidden_neurons, activation=PARAMETERS.activation, kernel_regularizer=PARAMETERS.kernel_regularizer, 
                    kernel_initializer=PARAMETERS.kernel_initializer, name='hidden'))
    
    #Add output layer
    model.add(Dense(len(Y[0]), activation='linear', kernel_initializer=PARAMETERS.kernel_initializer, name='output'))
    
    #Compile the model
    model.compile(optimizer=PARAMETERS.optimizer, loss=PARAMETERS.loss, metrics=PARAMETERS.metrics, loss_weights=None, sample_weight_mode=None, 
                  weighted_metrics=None, target_tensors=None)
    
    #Fit the model to the data and store the history object
    training_history = model.fit(x=X, y=Y, batch_size=PARAMETERS.batch_size, epochs=PARAMETERS.max_cycles, verbose=1, 
                                 callbacks=CALLBACKS, validation_split=PARAMETERS.validation_percentage, validation_data=None, shuffle=False, 
                                 class_weight=None, sample_weight=None, initial_epoch=0, steps_per_epoch=None, validation_steps=None)
    
    #Store away the training history file
    with open(TRAINING_HISTORY_FILE, 'wb') as dump:
        pickle.dump(training_history, dump)
    
    return model

# TL_reduced, complete model (CM)
def ANN_TL_reduced_CM(X, Y, PARAMETERS, FILE_PATH, SOURCE_MODEL):
    
    #Import the necessary external modules
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import ModelCheckpoint, EarlyStopping
    from os.path import join
    import pickle

    
    # Create filepath for the model storage
    MODEL_FILE = join(FILE_PATH, "CM_model.hdf5")
    TRAINING_HISTORY_FILE = join(FILE_PATH, "CM_hist.json")
    
    # Add the checkpoint for the call back list
    CALLBACKS = []
    CHECKPOINT = ModelCheckpoint(MODEL_FILE, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='min', period=1)
    EARLYSTOPPING = EarlyStopping(monitor='val_loss', min_delta=0, patience=100, verbose=1, mode='auto', baseline=None, restore_best_weights=True)
    CALLBACKS.append(CHECKPOINT)
    CALLBACKS.append(EARLYSTOPPING)
    
    # Load the Source Domain Model
    model = SOURCE_MODEL
    
    #Compile the model
    model.compile(optimizer=PARAMETERS.optimizer, loss=PARAMETERS.loss, metrics=PARAMETERS.metrics, loss_weights=None, sample_weight_mode=None, 
                  weighted_metrics=None, target_tensors=None)
    
    #Fit the model to the data and store the history object
    training_history = model.fit(x=X, y=Y, batch_size=PARAMETERS.batch_size, epochs=PARAMETERS.max_cycles, verbose=1, 
                                 callbacks=CALLBACKS, validation_split=PARAMETERS.validation_percentage, validation_data=None, shuffle=False, 
                                 class_weight=None, sample_weight=None, initial_epoch=0, steps_per_epoch=None, validation_steps=None)
    
    #Store away the training history file
    with open(TRAINING_HISTORY_FILE, 'wb') as dump:
        pickle.dump(training_history, dump)
    
    return model

# TL_reduced, input and hidden layer (IHL)
def ANN_TL_reduced_IHL(X, Y, PARAMETERS, FILE_PATH, SOURCE_MODEL):
    
    #Import the necessary external modules
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import ModelCheckpoint, EarlyStopping
    from os.path import join
    import pickle
    import keras.backend as K
    
    # Create filepath for the model storage
    MODEL_FILE = join(FILE_PATH, "IHL_model.hdf5")
    TRAINING_HISTORY_FILE = join(FILE_PATH, "IHL_hist.json")
    
    # Add the checkpoint for the call back list
    CALLBACKS = []
    CHECKPOINT = ModelCheckpoint(MODEL_FILE, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='min', period=1)
    EARLYSTOPPING = EarlyStopping(monitor='val_loss', min_delta=0, patience=100, verbose=1, mode='auto', baseline=None, restore_best_weights=True)
    CALLBACKS.append(CHECKPOINT)
    CALLBACKS.append(EARLYSTOPPING)
    
    #Initialize the ANN-instance with the Sequential() class from Keras
    model = Sequential()

    #Add input layer
    model.add(SOURCE_MODEL.get_layer(name='input'))
    
    #Add Hidden Dense Layers
    model.add(SOURCE_MODEL.get_layer(name='hidden'))
    
    #Add output layer
    model.add(Dense(len(Y[0]), activation='linear', kernel_initializer=PARAMETERS.kernel_initializer, name='output'))
    
    #Compile the model
    model.compile(optimizer=PARAMETERS.optimizer, loss=PARAMETERS.loss, metrics=PARAMETERS.metrics, loss_weights=None, sample_weight_mode=None, 
                  weighted_metrics=None, target_tensors=None)
    
    #Fit the model to the data and store the history object
    training_history = model.fit(x=X, y=Y, batch_size=PARAMETERS.batch_size, epochs=PARAMETERS.max_cycles, verbose=1, 
                                 callbacks=CALLBACKS, validation_split=PARAMETERS.validation_percentage, validation_data=None, shuffle=False, 
                                 class_weight=None, sample_weight=None, initial_epoch=0, steps_per_epoch=None, validation_steps=None)
    
    #Store away the training history file
    with open(TRAINING_HISTORY_FILE, 'wb') as dump:
        pickle.dump(training_history, dump)
    
    return model

# TL_reduced, input layer (IL)
def ANN_TL_reduced_IL(X, Y, PARAMETERS, FILE_PATH, SOURCE_MODEL):
    
    #Import the necessary external modules
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import ModelCheckpoint, EarlyStopping
    from os.path import join
    import pickle
    import keras.backend as K
    
    # Create filepath for the model storage
    MODEL_FILE = join(FILE_PATH, "IL_model.hdf5")
    TRAINING_HISTORY_FILE = join(FILE_PATH, "IL_hist.json")
    
    # Add the checkpoint for the call back list
    CALLBACKS = []
    CHECKPOINT = ModelCheckpoint(MODEL_FILE, monitor='val_loss', verbose=1, save_best_only=True, save_weights_only=False, mode='min', period=1)
    EARLYSTOPPING = EarlyStopping(monitor='val_loss', min_delta=0, patience=100, verbose=1, mode='auto', baseline=None, restore_best_weights=True)
    CALLBACKS.append(CHECKPOINT)
    CALLBACKS.append(EARLYSTOPPING)
    
    #Initialize the ANN-instance with the Sequential() class from Keras
    model = Sequential()
    #Add input layer
    model.add(SOURCE_MODEL.get_layer(name='input'))
    
    #Add Hidden Dense Layers
    model.add(Dense(PARAMETERS.hidden_neurons, activation=PARAMETERS.activation, kernel_regularizer=PARAMETERS.kernel_regularizer, 
                    kernel_initializer=PARAMETERS.kernel_initializer, name='hidden'))
    
    #Add output layer
    model.add(Dense(len(Y[0]), activation='linear', kernel_initializer=PARAMETERS.kernel_initializer, name='output'))
    
    #Compile the model
    model.compile(optimizer=PARAMETERS.optimizer, loss=PARAMETERS.loss, metrics=PARAMETERS.metrics, loss_weights=None, sample_weight_mode=None, 
                  weighted_metrics=None, target_tensors=None)
    
    #Fit the model to the data and store the history object
    training_history = model.fit(x=X, y=Y, batch_size=PARAMETERS.batch_size, epochs=PARAMETERS.max_cycles, verbose=1, 
                                 callbacks=CALLBACKS, validation_split=PARAMETERS.validation_percentage, validation_data=None, shuffle=False, 
                                 class_weight=None, sample_weight=None, initial_epoch=0, steps_per_epoch=None, validation_steps=None)
    
    #Store away the training history file
    with open(TRAINING_HISTORY_FILE, 'wb') as dump:
        pickle.dump(training_history, dump)
    
    return model

#Making the prediction for X_Test based on a provided model
def Y_predict(model, X_Test):
    
    #Get the prediction
    Y_Test_Pred_Scaled = model.predict(x=X_Test)
    
    #Return the numpy array
    return Y_Test_Pred_Scaled