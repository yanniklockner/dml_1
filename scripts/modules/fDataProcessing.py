# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 17:45:57 2019

@author: lockner
"""

#Randomly draw datapoint from the lists and create the data chunks
def generate_data_chunks(X_Scaled, Y_Scaled, k_folds):
    
    #Import necessary external modules
    import random
    import math

    #Zip X_Scaled and Y_Scaled to be able to shuffle the elements
    data = list(zip(X_Scaled, Y_Scaled))
    
    #Shuffle the list; return none
    random.shuffle(data)
    
    #Divide the data again
    X_Scaled = []
    Y_Scaled = []
    for i in range(len(data)):
        X_Scaled.append(data[i][0].tolist())
        Y_Scaled.append(data[i][1].tolist())
        
    #Define how many data points one chunk should contain
    data_points_per_chunk = math.ceil(len(X_Scaled)/k_folds)
    
    #Initialize dictionary to store the chunks
    data_chunks = {key:{'X':{}, 'Y':{}} for key in range(k_folds)}
    
    #Use the key as counting number
    for key in data_chunks:
        
        #If the part of the list to be stored doesnt exceed the list of the length
        if len(X_Scaled) >= (key+1)*data_points_per_chunk:
            
            #Define point in list
            temp_list_point = key*data_points_per_chunk
            temp_list_point_2 = (key+1)*data_points_per_chunk
            
            #Store the data
            data_chunks[key]['X'] = X_Scaled[temp_list_point:temp_list_point_2]
            data_chunks[key]['Y'] = Y_Scaled[temp_list_point:temp_list_point_2]
            
        #If the part to be stored does exceed the list of the length
        else:
            
            #Define point in list
            temp_list_point = key*data_points_per_chunk
            
            #Store the data
            data_chunks[key]['X'] = X_Scaled[temp_list_point:-1]
            data_chunks[key]['Y'] = Y_Scaled[temp_list_point:-1]
            
    #Return the dictionary
    return data_chunks

#Assemble the number of k_folds to the corresponding number of training and test data set
#Annotation: external input for training data is always minimum that the software tries to match as closely as possible
#Annotation: Since it is yet unclear how the StratifiedKFold class works it might not be necessary to use this function. However, it is there.
def training_test_splits(data_chunks, tt_split):
    
    #import the necessary external modules
    import math
    
    #Initialize the dictionary
    data_split_dict = {key : {} for key in data_chunks}
    
    #Determine the int of training splits
    training_splits = math.ceil(len(data_chunks)*tt_split)
    
    #Dictionary instances must be initialized prior to iterating on the dictionary, otherwise KeyErrors
    for key in data_split_dict:
        
        data_split_dict[key]['X'] = {}
        data_split_dict[key]['X']['TRAIN'] = []
        data_split_dict[key]['X']['TEST'] = []
        data_split_dict[key]['Y'] = {}
        data_split_dict[key]['Y']['TRAIN'] = []
        data_split_dict[key]['Y']['TEST'] = []
    
    for key in data_split_dict:
        
        #Determining the number of folds that have already been added to the training data set
        count = 0
        #Determining a ticker
        ticker = 0
        
        #Do this as long as we did not yet add the required number of data splits to the training set
        while ticker <= training_splits:
        
            #Proceed normally if the key plus count does not exceed the dictionary length
            if key+count < len(data_split_dict):
                
                #Generate the temporary key
                temp_key = key+count
                
                #Add the data splits to the training data set
                data_split_dict[temp_key]['X']['TRAIN'].extend(data_chunks[temp_key]['X'])
                data_split_dict[temp_key]['Y']['TRAIN'].extend(data_chunks[temp_key]['Y'])
                
                #Counting up the ticker
                ticker += 1
                #Counting up the count
                count += 1
            
            #Go back to the beginning of the keys in data_splits if the counter would count over the kength of the dictionary
            elif key+count >= len(data_split_dict):
                
                #set the counter to "-key" as to start at the beginning again (so at "key == 0")
                count = -key
        
        #Store the test splits, the difference between trainings_splits and len(data_splits) is the resulting number of test_splits
        while ticker <= len(data_chunks):
            
            #Proceed normally if the key plus count does not exceed the dictionary length
            if key+count < len(data_split_dict):
            
                #Generate the temporary key
                temp_key = key+count
                
                #Add the data splits to the test data set
                data_split_dict[temp_key]['X']['TEST'].extend(data_chunks[temp_key]['X'])
                data_split_dict[temp_key]['Y']['TEST'].extend(data_chunks[temp_key]['Y'])
                
                #Counting up the ticker
                ticker += 1
                #Counting up the count
                count += 1
            
            #Go back to the beginning of the keys in data_splits if the counter would count over the kength of the dictionary
            elif key+count >= len(data_split_dict):
                
                #set the counter to "-key" as to start at the beginning again (so at "key == 0")
                count = -key
    
    #Return the dictionary
    return data_split_dict
    

#Assemble for a leave-one-partition-out training and test data set
def leave_one_out_split(X_Scaled, Y_Scaled):
    
    #Initialize the dictionary
    leave_one_out_splits = {key : {'X':{}, 'Y':{}} for key in range(len(X_Scaled))}
    
    #Initialize lists and store the single test data points
    for index1 in range(len(X_Scaled)):
        leave_one_out_splits[index1]['X']['TRAIN'] = []
        leave_one_out_splits[index1]['X']['TEST'] = X_Scaled[index1]
        leave_one_out_splits[index1]['Y']['TRAIN'] = []
        leave_one_out_splits[index1]['Y']['TEST'] = Y_Scaled[index1]
        
        #Store the training datapoints in the corresponding lists. The test data point should not be stored
        for index2 in range(len(X_Scaled)):
            if index2 != index1:
                leave_one_out_splits[index1]['X']['TRAIN'].append(X_Scaled[index2])
                leave_one_out_splits[index1]['Y']['TRAIN'].append(Y_Scaled[index2])        
    
    #Return the dictionary
    return leave_one_out_splits


#Scale the data 
def data_scaling(X_unscaled, Y_unscaled):
    
    #Import necessary external function
    from sklearn.preprocessing import MinMaxScaler
    
    #Create scaler-instance and fit X_unscaled and Y_unscaled to the value range
    scaler = MinMaxScaler(feature_range=(-1, 1))
    X_scaled = scaler.fit_transform(X_unscaled)
    Y_scaled = scaler.fit_transform(Y_unscaled)
    
    #Return the arrays and the scaler-instance
    return X_scaled, Y_scaled, scaler


#Un-scale the data
def unscaling(Y_pred_scaled, Y_test_scaled, X_Test, scaler):
    
    #Un-fit the data with the scaler-instance
    Y_pred_unscaled = scaler.inverse_transform(Y_pred_scaled)
    Y_test_unscaled = scaler.inverse_transform(Y_test_scaled)
    X_test_unscaled = scaler.inverse_transform(X_Test)
    
    #Return the arrays
    return Y_pred_unscaled, Y_test_unscaled, X_test_unscaled

#store the analysis data from each initialization on the lowest level
def store_analysis_data(training_history, model, scaler, X_Test, Y_Test, HPTR, hp, split, initialization):
    
    #import necessary external modules
    import fANN
    from sklearn.metrics import r2_score
    import numpy as np
    
    #Initialize a subdictionary for all chosen quality parameters, represented in the history object
    HPTR[hp][split][initialization] = {key : None for key in training_history.history}
    #Save the quality measures from the training in the corresponding dictionary; store the whole training list set to be able to plot
    for key in HPTR[hp][split][initialization]:
        HPTR[hp][split][initialization][key] = training_history.history[key]
    
    #Get the prediction for the test dataset
    Y_Test_Pred_Scaled = fANN.Y_predict(model, X_Test)
    
    #Save the predictions (Scaled!) in the HPTR dictionary retransform them from ndarray to list
    #Remove the inner list and transform to a flat list --> Y_Test_Pred_Scaled.tolist() = [[],[],...]; flat flist: [..,..,..]
    HPTR[hp][split][initialization]['Y_Test_Pred_Scaled'] = [i for [i] in Y_Test_Pred_Scaled.tolist()]
    
    #Save X_Test (Scaled!) in the HPTR dictionary for easier data access retransform them from ndarray to list
    HPTR[hp][split][initialization]['Y_Test_Scaled'] = [i for [i] in Y_Test.tolist()]
    
    #Unscale the data
    Y_Test_Pred_Unscaled, Y_Test_Unscaled, X_Test_Unscaled = unscaling(Y_Test_Pred_Scaled, Y_Test, X_Test, scaler)
    #Save the data to HPTR and retransform them from ndarray to list
    HPTR[hp][split][initialization]['Y_Test_Pred_Unscaled'] = [i for [i] in Y_Test_Pred_Unscaled.tolist()]
    HPTR[hp][split][initialization]['Y_Test_Unscaled'] = [i for [i] in Y_Test_Unscaled.tolist()]
    HPTR[hp][split][initialization]['X_Test_Unscaled'] = X_Test_Unscaled.tolist()
    
    #Save R2-score
    HPTR[hp][split][initialization]['R2_score'] = r2_score(Y_Test_Unscaled, Y_Test_Pred_Unscaled)
    
    #return the dictionary
    return HPTR

#Generate key figures for each data split for a given hyperparametercombination and store them in the HPTR dictionary
def store_analysis_data_2(HPTR, hp, split, initialization, training_history):
    
    #import the necessary external modules
    import statistics as sc
    
    #initialize a temporary dictionary containing the metrics by defining the containing metrics (create list)
    keys_temp = list(HPTR[hp][split][initialization].keys())
    
    #remove the data point values such as X_Test_Unscaled
    #Annotation: Could be done in the following double loop by excluding string-keys with key[0] == 'X' or 'Y'
    remove_temp = ['Y_Test_Pred_Scaled','Y_Test_Scaled','Y_Test_Pred_Unscaled','Y_Test_Unscaled','X_Test_Unscaled']
    for i in range(len(remove_temp)):
        keys_temp.remove(remove_temp[i])
    
    #Create a data dictionary for the remaining quality metrics
    metrics_temp = {keys_temp[key] : [] for key in range(len(keys_temp))}
    
    #Append the values of the split by looping over the existing metrics (in metrics_temp) and the initializations and then
    #appending the specific KNN.key value from the HPTR dictionary to the temporary dictionary
    #'key' stands for the metrics, such as 'R2_score'
    for key in metrics_temp:
        for KNN in HPTR[hp][split]:
            #store only the last entry of the training history metrics but the whole scalar list of the scalar metrics
            if key in training_history.history:
                metrics_temp[key].append(HPTR[hp][split][KNN][key][-1])
            else:
                metrics_temp[key].append(HPTR[hp][split][KNN][key])
        
    #Initializing a new sub-sub dictionary in HPTR for saving the results and saving the results directly by storing the sc.mean() value
    #of the corresponding entries for each key in the sub-sub dictionary!
    HPTR[hp][split]['average'] = {key : sc.mean(metrics_temp[key]) for key in metrics_temp}
    
    #return the dictionary for possible further progress
    return HPTR, keys_temp

#Generate and store the key figures of each hp-combination in the HPTR dictionary
def store_analysis_data_3(HPTR, hp, keys_temp):
    
    #import the necessary external modules
    import statistics as sc
    
    #Generate a temporary metrics dictionary to store the average values
    metrics_temp = {keys_temp[key] : [] for key in range(len(keys_temp))}
    
    #Append all the average metric values for each data split of the specific hyperparameter combination to the metrics_temp dictionary
    for datasplit in HPTR[hp]:
        for metric in HPTR[hp][datasplit]['average']:
            metrics_temp[metric].append(HPTR[hp][datasplit]['average'][metric])
    
    #Create a sub-sub dictionary in HPTR to contain the average values of the hyperparameter split by simultaneously creating the dict,
    #and storing the corresponding key:value combinations
    HPTR[hp]['average'] = {key : sc.mean(metrics_temp[key]) for key in metrics_temp}
    
    #return the dictionary for possible further progress
    return HPTR, keys_temp
    
#Generate and store the key figures for the run in the HPTR dictionary
def store_analysis_data_4(HPTR, keys_temp):
    
    #import the necessary external modules
    import statistics as sc
    
    #Generate a temporary metrics dictionary to store the average values
    metrics_temp = {keys_temp[key] : [] for key in range(len(keys_temp))}
    
    #Append all the average metric values for each hyperparametercombination to the metrics_temp dictionary
    for hp in HPTR:
        for metric in HPTR[hp]['average']:
            metrics_temp[metric].append(HPTR[hp]['average'][metric])
            
    #Create a sub dictionary to contain the average values of the run and simultaneously save the data
    HPTR['average'] = {key : sc.mean(metrics_temp[key]) for key in metrics_temp}
    
    #return the HPTR dictionary
    return HPTR
    
#Save the generated hyperparpameter combinations to JSON string
def hp_to_JSON(combinations_dict, run_file):
    
    #import necessary external modules
    import json
    import os.path
    
    #replace the regularizer expression by JSON-serializable string
    #Select only the expression before the opening of the argument parenthesis
    temp_dict = combinations_dict.copy()
    for hp in temp_dict:
        temp_dict[hp]['regularizer'] = str(temp_dict[hp]['regularizer']).split('(')[0]
    
    #Save the dictionary
    with open(os.path.join("runs",run_file,"Hyperparameter-Kombinationen") + ".json", "w") as dump:
        json.dump(temp_dict, dump)
        
    #return nothing
        
#Create an unique timestamp
def get_timestamp():
    
    #import necessary external modules
    import datetime
    
    #create a timestamp variable and prepare for storing as file name in windows directory
    run_file = str(datetime.datetime.utcnow())
    run_file = run_file.replace(':','-')
    run_file = run_file.replace(' ','_')
    run_file = run_file.replace('.','_')
    
    #return the timestamp
    return run_file
