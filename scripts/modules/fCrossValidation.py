# -*- coding: utf-8 -*-
"""
Created on Mon Jun 25 16:58:47 2018

@author: Yannik Lockner
"""

from sklearn.model_selection import train_test_split
import random
import math
import numpy as np

def k_fold_creation(X, Y, k_folds, testPartition):

    k_fold_dict = {}
    num = 1
    while num <= k_folds:
        k_fold_dict[num] = {}
        X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size = testPartition, shuffle = True)
        k_fold_dict[num]["X_train"] = X_train
        k_fold_dict[num]["X_test"] = X_test
        k_fold_dict[num]["Y_train"] = Y_train
        k_fold_dict[num]["Y_test"] = Y_test
        num += 1
    
    return k_fold_dict


def foldX(X, Y, k):
    
    #zip observations and initialize
    zippedObservations = list(zip(X,Y))
    random.shuffle(zippedObservations)
    X_un = [x[0] for x in zippedObservations]
    Y_un = [y[1] for y in zippedObservations]
    obsPerFold = round(len(X)/k)
    dist_dict = {}
    k_fold_dict = {}
    
    #change datatype
    X = []
    for array in X_un:
        element = list(array)
        X.append(element)
    Y= []
    for array in Y_un:
        element = list(array)
        Y.append(element)

    #slice in lists, store in dict
    #pay attention, python drops observations if requested number of observations is not available
    for i in range(k):
        dist_dict[i] = {}
        if len(X) > obsPerFold:
            dist_dict[i]["X"] = X[0:obsPerFold]
            dist_dict[i]["Y"] = Y[0:obsPerFold]
            X = X[obsPerFold:]
            Y = Y[obsPerFold:]
        else:
            dist_dict[i]["X"] = X
            dist_dict[i]["Y"] = Y
    
    #generate combinations
    for k in dist_dict:
        k_fold_dict[k] = {}
        #####für JSON-Erzeugung hier noch kein np.array
#        k_fold_dict[k]["X_test"] = dist_dict[k]["X"]
#        k_fold_dict[k]["Y_test"] = dist_dict[k]["Y"]
        k_fold_dict[k]["X_test"] = np.array(dist_dict[k]["X"])
        k_fold_dict[k]["Y_test"] = np.array(dist_dict[k]["Y"])
        X_train = []
        Y_train = []
        for j in dist_dict:
            if k != j:
                X_train += dist_dict[j]["X"]
                Y_train += dist_dict[j]["Y"]
        #hier noch nicht in Array transformieren, da noch weitere Listenoperation warten
        k_fold_dict[k]["X_train"] = X_train
        k_fold_dict[k]["Y_train"] = Y_train
    
    return k_fold_dict, dist_dict


def devideTrainValTest(X_complete, Y_complete, testPartition):
    
    if len(X_complete) == len(Y_complete):
        
        #Datatype changing because np.ndarray does not support .index()
        X_complete_list = list(X_complete)
        X_complete = []
        for array in X_complete_list:
            element = list(array)
            X_complete.append(element)
        Y_complete_list = list(Y_complete)
        Y_complete = []
        for array in Y_complete_list:
            element = list(array)
            Y_complete.append(element)
        
        #Back to normal procedure
        train_length = math.ceil(len(X_complete) * (1-testPartition))
        X_train_val = []
        Y_train_val = []
        for k in range(train_length):
            element_X = random.choice(X_complete)
            coordinate = X_complete.index(element_X)
            element_Y = Y_complete[coordinate]
            X_train_val.append(element_X)
            Y_train_val.append(element_Y)
            X_complete.remove(element_X)
            Y_complete.remove(element_Y)
        X_test = X_complete
        Y_test = Y_complete
        
        print("Es wurden" +str(len(X_test))+" Beobachtungen dem Testdatensatz zugeteilt, genauso wie"+str(len(Y_test))+" Labels.")
        print("Gefordert wurde, dass"+str(testPartition*100)+" Prozent des Datensatzes als Testdaten verwendet werden. Tatsächlich werden aufgrund eines potentiellen Rundungsfehlers"+str((len(X_test)/len(X_train_val))*100)+ "Prozent als Testdaten eingesetzt.")
        
        #Make sure, no element from X_test is in X_train_val
        for element in X_test:
            for other in X_train_val:
                if element == other:
                    return print("ERROR: In X_test befinden sich Elemente, die auch in X_train_val sind!")
        
        #Re-array-ing beccause of list-ing from above
        X_train_val = np.array(X_train_val)
        Y_train_val = np.array(Y_train_val)
        X_test = np.array(X_test)
        Y_test = np.array(Y_test)
        
        return X_train_val, Y_train_val, X_test, Y_test
        
    else:
        print("PROGRAM ERROR: Unterschiedliche Länge der Listen für die Beobachtungen und Labels!")
        

#dict structure: externalKEY: ExternalTestX, ExternalTestY, ExternalTrainX, ExternalTrainY
def loocv(X_complete, Y_complete):
    
    #Datatype changing because np.ndarray does not support .index()
    X_complete_list = list(X_complete)
    X_complete = []
    for array in X_complete_list:
        element = list(array)
        X_complete.append(element)
    Y_complete_list = list(Y_complete)
    Y_complete = []
    for array in Y_complete_list:
        element = list(array)
        Y_complete.append(element)
    
    #Selecting the folds
    combinations = list(zip(X_complete, Y_complete))
    externalCV = {}
    for (a,b) in combinations:
        externalCV[combinations.index((a,b))] = []
        lstA, lstB = [], []
        for (c,d) in combinations:
            if (c,d) != (a,b):
                lstA.append(c)
                lstB.append(d)
        externalCV[combinations.index((a,b))].append(a)
        externalCV[combinations.index((a,b))].append(b)
        externalCV[combinations.index((a,b))].append(lstA)
        externalCV[combinations.index((a,b))].append(lstB)
    
    return externalCV


#dict structure: externalKEY: internalKEY: InternalTestX, InternalTestY, InternalTrainX, InternalTrainY
def loocvINTERNAL(externalCV):
    
    internalCV = {}
    for step in externalCV:
        combinations = list(zip(externalCV[step][2], externalCV[step][3]))
        internalCV[step] = {}
        for (a,b) in combinations:
            internalCV[step][combinations.index((a,b))] = []
            lstA, lstB = [], []
            for (c,d) in combinations:
                if (c,d) != (a,b):
                    lstA.append(c)
                    lstB.append(d)
            internalCV[step][combinations.index((a,b))].append(a)
            internalCV[step][combinations.index((a,b))].append(b)
            internalCV[step][combinations.index((a,b))].append(lstA)
            internalCV[step][combinations.index((a,b))].append(lstB)
            
    return internalCV

#Fix Validating Data by shuffling through the data list for each iteration
def shuffleFold(k_fold_dict):
    
    #initialization
    train_shuffle_dict = {}
    for fold in k_fold_dict:
        train_shuffle_dict[fold] = {}
    for element in train_shuffle_dict:
        for fold in k_fold_dict:
            train_shuffle_dict[element][fold] = {}
    
    for element in train_shuffle_dict:
        X_train = k_fold_dict[element]["X_train"]
        Y_train = k_fold_dict[element]["Y_train"]
        listsplit = round(len(X_train)/len(k_fold_dict))
        for fold in train_shuffle_dict[element]:
            X1 = X_train[:listsplit]
            X2 = X_train[listsplit:]
            Y1 = Y_train[:listsplit]
            Y2 = Y_train[listsplit:]
            #print(X1)
            #print(X2)
            X_train = X2 + X1
            Y_train = Y2 + Y1
            train_shuffle_dict[element][fold]["X_train"] = np.array(X_train)
            train_shuffle_dict[element][fold]["Y_train"] = np.array(Y_train)
    
    return train_shuffle_dict


def TwentyFoldScarce(X, Y, k):
    
    #zip observations and initialize
    zippedObservations = list(zip(X,Y))
    random.shuffle(zippedObservations)
    X_un = [x[0] for x in zippedObservations]
    Y_un = [y[1] for y in zippedObservations]
    obsPerFold = round(len(X)/k)
    dist_dict = {}
    k_test_dict = {}
    
    #change datatype
    X = []
    for array in X_un:
        element = list(array)
        X.append(element)
    Y= []
    for array in Y_un:
        element = list(array)
        Y.append(element)

    #slice in lists, store in dict
    #pay attention, python drops observations if requested number of observations is not available
    for i in range(k):
        dist_dict[i] = {}
        if len(X) > obsPerFold:
            dist_dict[i]["X"] = X[0:obsPerFold]
            dist_dict[i]["Y"] = Y[0:obsPerFold]
            X = X[obsPerFold:]
            Y = Y[obsPerFold:]
        else:
            dist_dict[i]["X"] = X
            dist_dict[i]["Y"] = Y
    
    #generate data combinations
    for m in dist_dict:
        k_test_dict[m] = {}
        X_train = []
        X_test = []
        Y_train = []
        Y_test = []
        for n in dist_dict:
            if n <= m:
                X_train += dist_dict[n]["X"]
                Y_train += dist_dict[n]["Y"]
            else:
                X_test += dist_dict[n]["X"]
                Y_test += dist_dict[n]["Y"]
        k_test_dict[m]["X_train"] = X_train
        k_test_dict[m]["Y_train"] = Y_train
        k_test_dict[m]["X_test"] = X_test
        k_test_dict[m]["Y_test"] = Y_test
        
            
#        #####für JSON hier noch kein np.array
#        k_fold_dict[k]["X_test"] = dist_dict[k]["X"]
#        k_fold_dict[k]["Y_test"] = dist_dict[k]["Y"]
#        k_test_dict[k]["X_test"] = np.array(dist_dict[k]["X"])
#        k_test_dict[k]["Y_test"] = np.array(dist_dict[k]["Y"])
#        X_train = []
#        Y_train = []
#        for j in dist_dict:
#            if k != j:
#                X_train += dist_dict[j]["X"]
#                Y_train += dist_dict[j]["Y"]
#        #hier noch nicht in Array transformieren, da noch weitere Listenoperation warten
#        k_test_dict[k]["X_train"] = X_train
#        k_test_dict[k]["Y_train"] = Y_train
    
    return k_test_dict, dist_dict