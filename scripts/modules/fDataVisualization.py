# -*- coding: utf-8 -*-
"""
Created on Tue May  7 13:32:48 2019

@author: lockner
"""

#Basic Data visualization
def show_bokeh(HPTR, directory):
    
    ##TODO##
    #After loading the HPTR dictionary from databse (windows for now) int-keys are transformed to string. Obviously no quick fix.
    #Maybe change keys to string directly to avoid problems after loading
    #Flexibilize the function to be able to take direct python objects or loaded JSONs
        
    #Import the necessary external modules
    from bokeh.io import output_file
    from bokeh.layouts import gridplot
    from bokeh.plotting import figure, show
    from bokeh.models import Plot
    from bokeh.models import BoxSelectTool
    from bokeh.resources import CDN
    from bokeh.embed import file_html
    
    #The figure will be rendered in a static HTML file called output_file_test.html
    output_file('data_visualization_by_bokeh.html', 
                title='Test Diagram')
    
    ##TODO##
    #add tooltip
    #Make it nicer lol
    TOOLTIPS = [("Trainingsepisode", "$x"), ("Qualität", "$y")]
    
    ##TODO##
    #Make it available to choose which Data should be plotted; must be done by external input
    
    #Set up a generic figure() object with some of the available arguments. For more consider the bokeh online documentation
    fig = figure(background_fill_color = 'lightgray',
                 background_fill_alpha = 0.5,
                 x_axis_label = 'training epoch',
                 x_range = (-10,len(HPTR[0][0][0]['val_loss'])+10),
                 y_axis_label = 'KNN-quality',
                 #y_range = 'auto',
                 plot_height = 800,
                 plot_width = 800,
                 title_location = 'above',
                 title = 'KNN Training',
                 toolbar_location = 'below',
                 tooltips = TOOLTIPS)
    
    fig2 = figure(background_fill_color = 'lightgray',
                 background_fill_alpha = 0.5,
                 x_axis_label = 'training epoch',
                 x_range = (-10,len(HPTR[0][0][0]['val_loss'])+10),
                 y_axis_label = 'KNN-quality',
                 #y_range = 'auto',
                 plot_height = 800,
                 plot_width = 800,
                 title_location = 'above',
                 title = 'KNN Training',
                 toolbar_location = 'below',
                 tooltips = TOOLTIPS)
    
    #Choose display format: .line for lines, available are a lot of different visuals such as circles, bars and so on. Consider documentation
    fig.line([i for i in range(len(HPTR[0][0][0]['val_mean_squared_error']))], HPTR[0][0][0]['val_mean_squared_error'], line_color='red', line_width=3, legend='Validation MSE')
    fig.line([i for i in range(len(HPTR[0][0][0]['mean_squared_error']))], HPTR[0][0][0]['mean_squared_error'], line_color='red', line_width=1, line_dash='dotted', legend='Training MSE')
    
    fig2.line([i for i in range(len(HPTR[0][0][0]['val_loss']))], HPTR[0][0][0]['val_loss'], line_color='blue', line_width=3, legend='Validation Loss')
    fig2.line([i for i in range(len(HPTR[0][0][0]['loss']))], HPTR[0][0][0]['loss'], line_color='green', line_width=1, line_dash='dashed', legend='Training Loss')
    
    #set grid plot design
    plot = gridplot([[fig, fig2]])
    
    #save file
    html = file_html(plot, CDN)
    with open(directory+"\plot.html", 'w') as savefile:
        savefile.write(html)
    
    #Create the Graph / File
    show(plot)