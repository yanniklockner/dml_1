# -*- coding: utf-8 -*-
"""
Created on Tue Jun 26 17:15:18 2018

@author: Yannik Lockner
"""

import random

#Gibt eine Anzahl an Hyperparameter-Kombinationen aus, wobei nicht gewährleistet werden kann, dass die Kombinationen nicht doppelt vorkommen
def hyper(hyperParameterDict, hyperCombinations):
    
    hyperComDict = {}
    
    hyperCount = 1
    
    while hyperCount <= hyperCombinations:
        hyperComDict[hyperCount] = []
        for parameter in hyperParameterDict:
            hyperComDict[hyperCount].append(random.choice(hyperParameterDict[parameter]))
        hyperCount += 1
    
    return hyperComDict


#Gibt eine Anzahl an unterschiedlichen Hyperparameter-Kombinationen aus
def hyperX(hyperParameterDict, hyperCombinations):
    
    hyperComDict = {}
    
    hyperCount = 0
    
    while hyperCount < hyperCombinations:
        hyperComDict[hyperCount] = []
        IN = False
        while IN == False:
            temp_list = []
            for parameter in hyperParameterDict:
                temp_list.append(random.choice(hyperParameterDict[parameter]))
            val = list(hyperComDict.values())
            if temp_list in val:
                continue
            else:
                hyperComDict[hyperCount] = temp_list
                hyperCount += 1
                IN = True
    
    return hyperComDict