# -*- coding: utf-8 -*-
"""
Created on Fri Aug 17 14:08:31 2018

@author: Yannik Lockner
"""

from sklearn.decomposition import PCA, KernelPCA
import numpy as np
from sknn import ae
from keras.layers import Input, Dense
from keras.models import Model

def kernelpca(X):
    
    kpca = KernelPCA(kernel = "sigmoid", fit_inverse_transform=True, n_components=12)
    X_kpca = kpca.fit_transform(X)
    X_img_kpca = kpca.inverse_transform(X_kpca)
    
    return kpca, X_kpca, X_img_kpca

def linearpca(X):
    
    pca = PCA(n_components = 11)
    X_pca = pca.fit_transform(X)
    X_img_pca = pca.inverse_transform(X_pca)
    
    return pca, X_pca, X_img_pca

def autoencoder(X):
    
    autoencoder = ae.AutoEncoder(layer=[ae.Layer("Tanh", units=13, name="Input"), ae.Layer("Tanh", units=12, name="Hidden"), ae.Layer("Tanh", units=13, name="Output")], learning_rate = 0.01,  n_iter = 1000)
    autoencoder.fit(X)
    X = autoencoder.transform(X)
    
    return autoencoder, X


def autoencoderKERAS(X):
    
    #####Setting the dimension of the data representation
    rep_dim = 12
    
    #####Input-Placeholder
    input_img = Input(shape=(13,))
    
    #####New Representation
    rep = Dense(rep_dim, activation="tanh")(input_img)
    
    #####Restoring to the old Data
    restored = Dense(len(X[0]), activation="tanh")(rep)
    
    #####Mapping the Input to its restored Values
    autoencoder = Model(input_img, restored)
    
    #####Mapping the Input to its representation
    representer = Model(input_img, rep)
    
    #####
    rep_input = Input(shape=(rep_dim,))
    
    #####
    restored_layer = autoencoder.ayer[-1]
    
    #####
    decoder = Model(rep_input, restored_layer(rep_input))
    
    
    
    autoencoder.compile(optimizer="rmsprop", loss="mean_squared_error")
    autoencoder.fit(X, X, epochs=1000, batch_size=len(X), shuffle = True, validation_split=0.16)





























