# -*- coding: utf-8 -*-
"""
Created on Sun Jun 24 15:02:23 2018

@author: Yannik Lockner
"""

import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#Does not parse the lego building block specific geometry information like nops or sockel_heigth and neither injection volume as it is the same as volume
def parsing_common_geometry_information(lego_geometry, heighth, DATA_PATH):
    
    import csv
    from os.path import join
    
    #Selektieren des richtigen Dateipfads
    if heighth == "original":
        file1 = '_Lego_001'
        file2 = '_Lego_002'
        name = '_Lego'
    if heighth == "hoch":
        file1 = "_Lego_hoch_001"
        file2 = "_Lego_hoch_002"  
        name = '_Lego_hoch'
    if heighth == "3fach":
        file1 = "_Lego_3fach_001"
        file2 = "_Lego_3fach_002"    
        name = '_Lego_3fach'
    if heighth == "flach":
        file1 = "_Lego_flach_001"
        file2 = "_Lego_flach_002"        
        name = '_Lego_flach'
    if heighth == "gedrittelt":
        file1 = "_Lego_gedrittelt_001"
        file2 = "_Lego_gedrittelt_002"
        name = '_Lego_gedrittelt'
        
    filepath1 = join(DATA_PATH, heighth, lego_geometry + file1)
    filepath2 = join(DATA_PATH, heighth, lego_geometry + file2)
    V = [filepath1, filepath2]
    
    XInput = []
    YInput = []
    
    temp_dict_result = {}
    
    for path in V:
    
        if path[-1] == "1":
            way = join(path, 'DOE1.txt')
        if path[-1] == "2":
            way = join(path, 'DOE2.txt')
            
        file = open(way)
        
        #Init lists
        liste = []
        cleanlist= []
        cleanierlist = []
        cleanliestlist = []
        testindexlist = []
        
        #Init dicts
        index_dict = {}
        temp_dict = {}
        geometry_dict = {}
        
        #erase lines with less then 5 split-entries or without "Test" in the index 0 position of the split-list
        for row in file:
            temp_array = row.split()
            if len(temp_array)>5 or "Test" in temp_array:
                liste.append(temp_array)
        
        #erase experiment design lines         
        for item in liste:
            count = 0
            for entry in item:
                if entry == "+" or entry == "-":
                    count +=1
            if count <=2:
                cleanlist.append(item)
        
        #erase experiment nr. 65
        for cleanse in cleanlist:
            count = 0
            for rub in cleanse:
                if rub == "0":
                    count +=1
            if not count > 2:
                cleanierlist.append(cleanse)
        
        #erase axis-description lines
        for superclean in cleanierlist:
            if not superclean[0] == "Nr":
                cleanliestlist.append(superclean)
        
        #determine the index postion of the test-start-lines in the list
        for sponge in cleanliestlist:
            if "Test" in sponge:
                testindexlist.append(cleanliestlist.index(sponge))
        
        #organize the line-indices of the tests in clenliestlist in index_dict
        for i in testindexlist:
            index_dict["Test " + cleanliestlist[i][1].split()[0][:-1]] = []
            j = i
            index_i = testindexlist.index(i)
            try:
                break_i = testindexlist[index_i+1]
            except:
                break_i = len(cleanliestlist)
            while j < break_i:
                index_dict["Test " + cleanliestlist[i][1].split()[0][:-1]].append(j)
                j += 1
                
        for element in index_dict:
            index_dict[element] = index_dict[element][1:]
            
        ### organize the test date in dictionary structure: 'Test': [Volumenstrom, Nachdruck, Schmelzetemperatur, Wandtemperatur, Nachdruckzeit, Kühlzeit ab Füllende, Vorlauftemperatur 1, Vorlauftemperatur 2, Vorlauftemperatur 3, Vorlauftemperatur 4]
        for element in index_dict:
            temp_dict[element] = []
            for entry in index_dict[element]:
                try:
                    temp_dict[element].append(float(cleanliestlist[entry][3]))
                except:
                    temp_dict[element].append(float(cleanliestlist[entry-1][3]))
            
        #Determine 'Oberfläche' und 'Volumen' for piece
        u = 1
        dimensions = open(path + "\\" + lego_geometry + file1 + ".txt")
        for row in dimensions:
            if u <= 10:
                try:
                    if row.split()[0] == "Oberfläche":
                        surface = float(row.split()[2])
                    if row.split()[0] == "Volumen":
                        volume = float(row.split()[2])
                except:
                    continue
            u += 1
        
        #Determine 'Kompaktheit' for piece
        surface_space = 4*math.pi*math.pow((3*volume)/(4*math.pi) , 2/3)
        compactness = float(surface_space / surface)
        
        #Add 'Oberfläche', 'Volumen' and 'Kompaktheit' to geometry_dict
        geometry_dict["surface"] = surface
        geometry_dict["volume"] = volume 
        geometry_dict["compactness"] = compactness
        
        #Add 'Fließweglänge', 'mittlere Wanddicke', 'minimale Wanddicke', 'maximale Wanddicke': [Volumenstrom, Nachdruck, Schmelzetemperatur, Wandtemperatur, Nachdruckzeit, Kühlzeit ab Füllende, Oberfläche, Volumen, Kompaktheit, Fließweglänge, mittlere Wanddicke, minimale Wanddicke, maximale Wanddicke]
        rowdict = {}
        with open(join(DATA_PATH, 'Geometrieinformationen\Legobausteine.csv'), 'r') as myfile:
            filereader = csv.reader(myfile, delimiter='\t')
            key = 1
            for row in filereader:
                rowdict[key] = row
                key += 1
                
        for element in rowdict:
            rowdict[element] = rowdict[element][0].split(";")
        
        #Write geometry parameters into geometry_dict
        for element in rowdict:
            if rowdict[element][0] == "Mittlere_Wanddicke":
                geometry_dict["average_thickness"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            if rowdict[element][0] == "Max_Wanddicke":
                geometry_dict["max_thickness"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            if rowdict[element][0] == "Min_Wanddicke":
                geometry_dict["min_thickness"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            if rowdict[element][0] == "Fliessweglaenge":
                geometry_dict["flow_path"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            #if rowdict[element][0] == "Schussvolumen":
                #geometry_dict["inj_vol"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            if rowdict[element][0] == "Oberflaeche/Volumen":
                geometry_dict["AV_ratio"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            if rowdict[element][0] == "Fliesswegwanddickenverhaeltnis":
                geometry_dict["flow_thick_ratio"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            if rowdict[element][0] == "Min_Wanddicke/Max_Wanddicke":
                geometry_dict["minmax_thick_ratio"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            if rowdict[element][0] == "PACYNA1: DÃ¼nnwÃ¤ndigkeit D":
                geometry_dict["P1"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            if rowdict[element][0] == "PACYNA2: Gestrecktheit G":
                geometry_dict["P2"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            if rowdict[element][0] == "PACYNA3: Volumensperrigkeit V":
                geometry_dict["P3"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            if rowdict[element][0] == "HaupttrÃ¤gheitsmomente1":
                geometry_dict["HT1"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            if rowdict[element][0] == "HaupttrÃ¤gheitsmomente2":
                geometry_dict["HT2"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            if rowdict[element][0] == "HaupttrÃ¤gheitsmomente3":
                geometry_dict["HT3"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            #if rowdict[element][0] == "Anzahl Noppen pro Reihe":
                #geometry_dict["nops"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            #if rowdict[element][0] == "Anzahl der Reihen":
                #geometry_dict["rows"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            #if rowdict[element][0] == "SockelhÃ¶he":
                #geometry_dict["sockel_heighth"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
            #if rowdict[element][0] == "NoppenhÃ¶he":
                #geometry_dict["nops_heighth"] = float(rowdict[element][rowdict[1].index(str(lego_geometry+name))].replace(',', '.'))
        
        #Add 'Bauteilgewicht' to temp_dict: 'Test': [Volumenstrom, Nachdruck, Schmelzetemperatur, Wandtemperatur, Nachdruckzeit, Kühlzeit ab Füllende, Oberfläche, Volumen, Komptaktheit, Fließweglänge, mittlere Wanddicke, minimale Wanddicke, maximale Wanddicke, Bauteilgewicht]
        e = 1
        if path[-1] == "1":
            while e <= len(temp_dict):
                if e < 10:
                    result = open(path + "\DOE1_00" + str(e) + ".txt")
                elif e < 100:
                    result = open(path +"\DOE1_0" + str(e) + ".txt")
                else:
                    result = open(path +"\DOE1_" + str(e) + ".txt")
                
                for line in result:
                    if "Bauteilgewicht" in line:
                        temp_dict["Test " + str(e)].append(float(line.split()[2]))
                    
                e += 1 
        if path[-1] == "2":
            while e <= len(temp_dict):
                if e < 10:
                    result = open(path + "\DOE2_00" + str(e) + ".txt")
                elif e < 100:
                    result = open(path +"\DOE2_0" + str(e) + ".txt")
                else:
                    result = open(path +"\DOE2_" + str(e) + ".txt")
                
                for line in result:
                    if "Bauteilgewicht" in line:
                        temp_dict["Test " + str(e)].append(float(line.split()[2]))
                    
                e += 1 
            
        #Transcribe to XInput, YInput
        for element in temp_dict:
            xinput = []
            for y in range(len(temp_dict[element])):
                xinput.append(temp_dict[element][y])
            XInput.append(xinput[:-1])
            YInput.append([xinput[-1]])
        
        #Update the overall temp_dict_result with temp_dict for this iteration
        temp_dict_result.update(temp_dict)
        print()
        
    return XInput, YInput, geometry_dict