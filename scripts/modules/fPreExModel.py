# -*- coding: utf-8 -*-
"""
Created on Tue Apr 23 18:57:28 2019

@author: lockner
"""

#Selecting a pre-existing model based on restrictions
def searchformodels(xi):
    
    #Initialize the dictionary
    models = {}
    
    #Check if suitable existing models are accessible
    ##EXAMPLE if existing_models == True:
        #do whatever
        
    #Load data into data structures
    ##EXAMPLE else
    #           somelogic = set.somelogic()
    #           db = fDS.loaddata(somelogic)
        
    #...
    
    #Return the dictionary
    return models