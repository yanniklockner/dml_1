# -*- coding: utf-8 -*-
"""
Created on Tue Apr 23 19:03:44 2019

@author: lockner
"""

#Building data structure, therefore extracting data from the database into python formats to make it compatile for training

def loaddata(somelogic):
    
    #%%
    #data structure
    db1 = {}
    db2 = {}
    db3 = {}
    db4 = {}
    db5 = {}
    
    #level 1 dict: keys are plastic materials, such as 'PP', 'PA6', 'PC' or 'ABS'
    ##EXAMPLE db1['PP'] = [db2]
    
    #level 2 dict: keys are component numbers, such as 'IKV0467' or 'VW3338'
    ##EXAMPLE db2['PP_lego1x1'] = [db3]
    
    #level 3 dict: keys are types of DoE, such as 'VFV77'
    ##EXAMPLE db3['PP_lego1x1_VFV77'] = [db4]
    
    #level 4 dict: keys are the experimental point, such as '4'
    ##EXAMPLE db4['PP_lego1x1_VFV77_1'] = [db5]
    
    #level 5 dict: keys are the used input parameter descriptions, such as 'mt' for melt temperature
    #values of level 5 dict are the values of the input machine parameters
    ##EXAMPLE db5['PP_lego1x1_VFV77_1_mt'] = 220
    
    return db1