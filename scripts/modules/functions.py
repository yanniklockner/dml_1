def toyBrickList():

    toy_brick_list = []

    toy_brick_size = ['1x1', '1x2', '2x1', '2x2', '3x1', '3x2', '4x1', '4x2', '6x1', '6x2', '8x1', '8x2']
    toy_brick_configuration = ['original', 'flach', 'hoch', 'gedrittelt', '3fach']

    for size in toy_brick_size:
        for configuration in toy_brick_configuration:
            toy_brick_list.append((size, configuration))

    return toy_brick_list

    