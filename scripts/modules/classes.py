# -*- coding: utf-8 -*-

import sys

class Parameters:
    def __init__(self, max_cycles, optimizer, activation, kernel_regularizer, 
                 kernel_initializer, number_of_hidden_layers, 
                 number_of_hidden_neurons, training_percentage, 
                 validation_percentage, testing_percentage, loss, batch_size, 
                 metrics):
        assert number_of_hidden_layers == 1, "Please choose only one hidden layer or edit classes.py"
        self.max_cycles = max_cycles
        self.optimizer = optimizer
        self.activation = activation
        self.kernel_regularizer = kernel_regularizer
        self.kernel_initializer = kernel_initializer
        self.hidden_layers = number_of_hidden_layers
        self.hidden_neurons = number_of_hidden_neurons
        self.training_percentage = training_percentage
        self.validation_percentage = validation_percentage
        self.testing_percentage = testing_percentage
        self.loss = loss
        self.batch_size = batch_size
        self.metrics = metrics
        
    def __str__(self):
        print(self)
            