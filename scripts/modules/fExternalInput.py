# -*- coding: utf-8 -*-
"""
Created on Tue Apr 23 18:33:08 2019

@author: lockner
"""

#external input parser

def external_input_parser_true():
    
    #TODO
    
    return xi

#Work-around if the external input connection is not yet established
def external_input_parser_false():
    
    #Initialize the dictionary
    xi = {}
    
    #Store the default values in the dictionary
    xi['newdata'] = False
    xi['allexistingdatapoints'] = True
    xi['giveaccesstoexistingmodels'] = False
    xi['qualityparameters'] = ['weight']
    xi['geometryparameters'] = ['volume', 'surface', 'compactness', 'max_flow_path', 'min_wall_thickness', 'mid_wall_thickness', 'max_wall_thickness']
    xi['materialparameters'] = []
    xi['maximaltrainingcycles'] = 2000
    xi['minimaloptimizationaccuracy'] = 0.002 #mse
    xi['hyperparameterstobetuned'] = ['optimizer', 'activation', 'hiddenlayers', 'hiddenlayer1_neurons', 'hiddenlayer2_neurons']
    xi['hyperparametertuning'] = True
    xi['hp_combinations'] = 1
    xi['k_folds'] = 5
    xi['repetitions_of_initialization'] = 2
    #0<=tt_split<=1.0 ; tt_split determines the minimum of training percentage!
    xi['tt_split'] = 0.8
    xi['loo_split'] = False
    xi['optimizationsettings'] = False
    xi['optimizationsettings_dict'] = {}
        
    #Return the dictionary   
    return xi
    
#Definins the optimizationssettings for the artifical neural networks
def optimization_settings(xi):
    
    #The boolean value 'True' indicates user input for the settings that differ from the default settings
    #ANNOTATION: Do not compute R² here but after prediction based on the individual results
    if xi['optimizationsettings'] == True:
        from tensorflow.python.ops.losses.losses_impl import mean_squared_error as TF_mean_squared_error
        from tensorflow.python.ops.metrics_impl import mean_absolute_error as TF_mean_absolute_error
        from tensorflow.python.ops.metrics_impl import mean_squared_error as TF_mean_squared_error
        from tensorflow.python.ops.metrics_impl import root_mean_squared_error as TF_root_mean_squared_error  
        from keras.callbacks import EarlyStopping
        
        #Initialize dictionary containing optimizationsettings
        OS = {}
        
        #Set Callback list
        #Annotation: set only early stop, as Modelcheckpoint requires filepath and this is depending on the number of models. Therefore set the filepath
        #in the ANN-function. Use timestamp
        early_stop = EarlyStopping(monitor='val_loss', min_delta=0, patience=50, verbose=0, mode='auto', 
                                   baseline=None, restore_best_weights=True)

        #Store default values in CS
        OS['loss'] = 'TF_mean_squared_error'
        OS['metrics'] = ['TF_mean_absolute_error', 'TF_mean_squared_error', 'TF_root_mean_squared_error']
        
        #Compare optimizationsettings_dict with OS and adjust values that differ from the default settings
        for key in OS:
            if OS[key] != xi['compilesettings_dict'][key]:
                OS[key] = xi['compilesettings_dict'][key]
        
        #Return the dictionary
        return OS
    
    #Defining the default values
    #ANNOTATION: Do not compute R² here but after prediction based on the individual results
    #Annotation: use strings for metrics, problems with tensorflow objects in keras compile-function
    if xi['optimizationsettings'] == False:
        #from tensorflow.python.ops.losses.losses_impl import mean_squared_error as TF_mean_squared_error
        #from tensorflow.python.ops.metrics_impl import mean_absolute_error as TF_mean_absolute_error
        #from tensorflow.python.ops.metrics_impl import mean_squared_error as TF_mean_squared_error
        #from tensorflow.python.ops.metrics_impl import root_mean_squared_error as TF_root_mean_squared_error
        from keras.callbacks import EarlyStopping
        from tensorflow.python.keras.callbacks import ModelCheckpoint
        
        #Initialize dictionary containing optimizationsettings
        OS = {}
        
        #Set Callback list
        #Annotation: set only early stop, as Modelcheckpoint requires filepath and this is depending on the number of models. Therefore set the filepath
        #in the ANN-function. Use timestamp
        early_stop = EarlyStopping(monitor='val_loss', min_delta=0, patience=50, verbose=0, mode='auto', 
                                   baseline=None, restore_best_weights=True)
        
        #Store the values
        OS['loss'] = 'mean_squared_error'
        OS['metrics'] = ['mean_absolute_error', 'mean_squared_error']
        OS['batch_size'] = None
        OS['verbose'] = 1
        OS['callbacks'] = early_stop #In ANN-function completed to: [early_stop, checkpoint]
        OS['validation_split'] = 0.2
        
        #Return the dictionary
        return OS
        
        
        