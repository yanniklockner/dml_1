# -*- coding: utf-8 -*-
"""
Created on Sun Jun 24 14:59:58 2018

@author: Yannik Lockner
"""

def neuralNetworkNCV(X_train, Y_train, X_test, fold_number, element, global_settings, combinationFROMhyperComDict, count, combination):
    
    from keras.optimizers import SGD, Adam, RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2, l1_l2
    
    #transcribe hyperparameterlist
    hypers = combinationFROMhyperComDict
    
    #Extract global settings
    abortCycles = global_settings["abortCycles"]
    performanceFunction = global_settings["performanceFunction"]
    maxTrainingCycles = global_settings["maxTrainingCycles"]
        
    #save modell
    filepath="model\myWeights_Run_" + str(fold_number) + "_" + str(element) + "_" + str(combination) + "_" + str(count+1) + ".hdf5"
    
    #Preparation for HyperParameter Tuning -- Optimizers
    if hypers[2] == 'sgdNesterov':
        optimizer = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    elif hypers[2] == 'adam':
        optimizer = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    elif hypers[2] == 'rmsprop':
        optimizer = RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)
    else:
        optimizer = hypers[2]
        
    #Preparation for HyperParamter Tuning  -- Regularizer
    if type(hypers[3]) == str:
        regularizer = l2(l=float(hypers[3]))
    if type(hypers[3]) == list:
        regularizer = l1_l2(l1=float(hypers[3][0]), l2=float(hypers[3][0]))
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'val_loss', min_delta = 0, verbose = 0, patience = abortCycles, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'val_loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and HyperParameter Tuning -- Number Hidden Layers and Neurons per layer
    if float(hypers[4]) == 0:
        modell = Sequential()
        modell.add(Dense(len(X_train[0]), init = hypers[0], W_regularizer = regularizer, input_dim = len(X_train[0]), activation = hypers[1], name='inputLayer'))
        modell.add(Dense(len(Y_train[0]), init = hypers[0], activation = 'linear'))    
        modell.compile(loss = performanceFunction, optimizer = optimizer, metrics = ['mse'])
        trainingHistory = modell.fit(X_train, Y_train, validation_split = global_settings["validation_split"], epochs=maxTrainingCycles, batch_size = len(X_train), callbacks=callback_list, verbose=0)
    if float(hypers[4]) == 1:
        modell = Sequential()
        modell.add(Dense(len(X_train[0]), init = hypers[0], W_regularizer = regularizer, input_dim = len(X_train[0]), activation = hypers[1], name='inputLayer'))
        modell.add(Dense(int(hypers[5]), init = hypers[0], W_regularizer = regularizer, input_dim = len(X_train[0]), activation = hypers[1], name='hiddenLayer1'))
        modell.add(Dense(len(Y_train[0]), init = hypers[0], activation = 'linear'))    
        modell.compile(loss = performanceFunction, optimizer = optimizer, metrics = ['mse'])
        trainingHistory = modell.fit(X_train, Y_train, validation_split = global_settings["validation_split"], epochs=maxTrainingCycles, batch_size = len(X_train), callbacks=callback_list, verbose=0)
    if float(hypers[4]) == 2:
        modell = Sequential()
        modell.add(Dense(len(X_train[0]), init = hypers[0], W_regularizer = regularizer, input_dim = len(X_train[0]), activation = hypers[1], name='inputLayer'))
        modell.add(Dense(int(hypers[5]), init = hypers[0], W_regularizer = regularizer, input_dim = len(X_train[0]), activation = hypers[1], name='hiddenLayer1'))
        modell.add(Dense(int(hypers[6]), init = hypers[0], W_regularizer = regularizer, input_dim = int(hypers[5]), activation = hypers[1], name='hiddenLayer2'))
        modell.add(Dense(len(Y_train[0]), init = hypers[0], activation = 'linear'))    
        modell.compile(loss = performanceFunction, optimizer = optimizer, metrics = ['mse'])
        trainingHistory = modell.fit(X_train, Y_train, validation_split = global_settings["validation_split"], epochs=maxTrainingCycles, batch_size = len(X_train), callbacks=callback_list, verbose=0)
    
    Y_pred_scaled = modell.predict(X_test)
    
    return trainingHistory, Y_pred_scaled


def neuralNetworkPerformance(X_train, Y_train, X_test, fold, global_settings, combinationFROMhyperComDict, count, combination):
    
    from keras.optimizers import SGD, Adam, RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2, l1_l2
    
    #transcribe hyperparameterlist
    hypers = combinationFROMhyperComDict
    
    #Extract global settings
    abortCycles = global_settings["abortCycles"]
    performanceFunction = global_settings["performanceFunction"]
    maxTrainingCycles = global_settings["maxTrainingCycles"]
        
    #save modell
    filepath="model\BestHyperInPartition" + str(fold) + ".hdf5"
    
    #Preparation for HyperParameter Tuning -- Optimizers
    if hypers[2] == 'sgdNesterov':
        optimizer = SGD(lr=0.01, decay=1e-6, momentum=0.9, nesterov=True)
    elif hypers[2] == 'adam':
        optimizer = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
    elif hypers[2] == 'rmsprop':
        optimizer = RMSprop(lr=0.001, rho=0.9, epsilon=None, decay=0.0)
    else:
        optimizer = hypers[2]
        
    #Preparation for HyperParamter Tuning  -- Regularizer
    if type(hypers[3]) == str:
        regularizer = l2(l=float(hypers[3]))
    if type(hypers[3]) == list:
        regularizer = l1_l2(l1=float(hypers[3][0]), l2=float(hypers[3][0]))
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'val_loss', min_delta = 0, verbose = 0, patience = abortCycles, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'val_loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and HyperParameter Tuning -- Number Hidden Layers and Neurons per layer
    if float(hypers[4]) == 0:
        modell = Sequential()
        modell.add(Dense(len(X_train[0]), init = hypers[0], W_regularizer = regularizer, input_dim = len(X_train[0]), activation = hypers[1], name='inputLayer'))
        modell.add(Dense(len(Y_train[0]), init = hypers[0], activation = 'linear'))    
        modell.compile(loss = performanceFunction, optimizer = optimizer, metrics = ['mse'])
        trainingHistory = modell.fit(X_train, Y_train, validation_split=0, epochs=maxTrainingCycles, batch_size = len(X_train), callbacks=callback_list, verbose=0)
    if float(hypers[4]) == 1:
        modell = Sequential()
        modell.add(Dense(len(X_train[0]), init = hypers[0], W_regularizer = regularizer, input_dim = len(X_train[0]), activation = hypers[1], name='inputLayer'))
        modell.add(Dense(int(hypers[5]), init = hypers[0], W_regularizer = regularizer, input_dim = len(X_train[0]), activation = hypers[1], name='hiddenLayer1'))
        modell.add(Dense(len(Y_train[0]), init = hypers[0], activation = 'linear'))    
        modell.compile(loss = performanceFunction, optimizer = optimizer, metrics = ['mse'])
        trainingHistory = modell.fit(X_train, Y_train, validation_split=0, epochs=maxTrainingCycles, batch_size = len(X_train), callbacks=callback_list, verbose=0)
    if float(hypers[4]) == 2:
        modell = Sequential()
        modell.add(Dense(len(X_train[0]), init = hypers[0], W_regularizer = regularizer, input_dim = len(X_train[0]), activation = hypers[1], name='inputLayer'))
        modell.add(Dense(int(hypers[5]), init = hypers[0], W_regularizer = regularizer, input_dim = len(X_train[0]), activation = hypers[1], name='hiddenLayer1'))
        modell.add(Dense(int(hypers[6]), init = hypers[0], W_regularizer = regularizer, input_dim = int(hypers[5]), activation = hypers[1], name='hiddenLayer2'))
        modell.add(Dense(len(Y_train[0]), init = hypers[0], activation = 'linear'))    
        modell.compile(loss = performanceFunction, optimizer = optimizer, metrics = ['mse'])
        trainingHistory = modell.fit(X_train, Y_train, validation_split=0, epochs=maxTrainingCycles, batch_size = len(X_train), callbacks=callback_list, verbose=0)
    
    Y_pred_scaled = modell.predict(X_test)
    
    return trainingHistory, Y_pred_scaled


def neuralNetwork1x1(X_complete, Y_complete):
    
    from keras.optimizers import RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2
        
    #save modell
    filepath="model\m1x1" +str(456) + ".hdf5"
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'loss', min_delta = 0, verbose = 0, patience = 50, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and HyperParameter Tuning -- Number Hidden Layers and Neurons per layer
    modell = Sequential()
    modell.add(Dense(len(X_complete[0]), init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='inputLayer'))
    modell.add(Dense(7, init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='hiddenLayer1'))
    modell.add(Dense(len(Y_complete[0]), init = "he_normal", activation = 'linear'))    
    modell.compile(loss = "mean_squared_error", optimizer = "rmsprop", metrics = ['mse'])
    trainingHistory = modell.fit(X_complete, Y_complete, validation_split=0, epochs=1000, batch_size = len(X_complete), callbacks=callback_list, verbose=0)
    
    return trainingHistory, modell


def neuralNetwork1x1Control(X_train, Y_train, fold, num):
    
    from keras.optimizers import RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2
        
    #save modell
    filepath="model\m1x1Control_Fold" + str(fold) + "_Num" + str(num) + ".hdf5"
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'val_loss', min_delta = 0, verbose = 0, patience = 50, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'val_loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and HyperParameter Tuning -- Number Hidden Layers and Neurons per layer
    modell = Sequential()
    modell.add(Dense(len(X_train[0]), init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_train[0]), activation = "tanh", name='inputLayer'))
    modell.add(Dense(7, init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_train[0]), activation = "tanh", name='hiddenLayer1'))
    modell.add(Dense(len(Y_train[0]), init = "he_normal", activation = 'linear'))    
    modell.compile(loss = "mean_squared_error", optimizer = "rmsprop", metrics = ['mse'])
    trainingHistory = modell.fit(X_train, Y_train, validation_split=0.16, epochs=1000, batch_size = len(X_train), callbacks=callback_list, verbose=0)
    
    return trainingHistory, modell


def neuralNetwork1x1FreezedLayer(X_train, Y_train, fold, num, oldModel):
    
    from keras.optimizers import RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2
        
    #save modell
    filepath="model\m1x1Control_Fold" + str(fold) + "_Num" + str(num) + ".hdf5"
    
    #Extract the layer from the oldModel object
    freezedLayer = oldModel.get_layer(name="inputLayer")
    freezedLayer.trainable = False
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'val_loss', min_delta = 0, verbose = 0, patience = 50, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'val_loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and instantization of freezedLayer
    modell = Sequential()
    modell.add(freezedLayer)
    modell.add(Dense(7, init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_train[0]), activation = "tanh", name='hiddenLayer1'))
    modell.add(Dense(len(Y_train[0]), init = "he_normal", activation = 'linear'))    
    modell.compile(loss = "mean_squared_error", optimizer = "rmsprop", metrics = ['mse'])
    trainingHistory = modell.fit(X_train, Y_train, validation_split=0.16, epochs=1000, batch_size = len(X_train), callbacks=callback_list, verbose=0)
    
    return trainingHistory, modell


def neuralNetwork1x1DoubleFreezedLayer(X_train, Y_train, fold, num, oldModel):
    
    from keras.optimizers import RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2
        
    #save modell
    filepath="model\m1x1Control_Fold" + str(fold) + "_Num" + str(num) + ".hdf5"
    
    #Extract the layer from the oldModel object
    freezedLayer = oldModel.get_layer(name="inputLayer")
    freezedLayer2 = oldModel.get_layer(name="hiddenLayer1")
    freezedLayer.trainable = False
    freezedLayer2.trainable = False
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'val_loss', min_delta = 0, verbose = 0, patience = 50, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'val_loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and instantization of freezedLayer
    modell = Sequential()
    modell.add(freezedLayer)
    modell.add(freezedLayer2)
    modell.add(Dense(len(Y_train[0]), init = "he_normal", activation = 'linear'))    
    modell.compile(loss = "mean_squared_error", optimizer = "rmsprop", metrics = ['mse'])
    trainingHistory = modell.fit(X_train, Y_train, validation_split=0.16, epochs=1000, batch_size = len(X_train), callbacks=callback_list, verbose=0)
    
    return trainingHistory, modell


def neuralNetwork1x1TrainableLayer(X_train, Y_train, fold, num, oldModel):
    
    from keras.optimizers import RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2
        
    #save modell
    filepath="model\m1x1Control_Fold" + str(fold) + "_Num" + str(num) + ".hdf5"
    
    #Extract the layer from the oldModel object
    trainableLayer = oldModel.get_layer(name="inputLayer")

    #callbacks
    stop_here_please = EarlyStopping(monitor = 'val_loss', min_delta = 0, verbose = 0, patience = 50, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'val_loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and instantization of freezedLayer
    modell = Sequential()
    modell.add(trainableLayer)
    modell.add(Dense(7, init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_train[0]), activation = "tanh", name='hiddenLayer1'))
    modell.add(Dense(len(Y_train[0]), init = "he_normal", activation = 'linear'))    
    modell.compile(loss = "mean_squared_error", optimizer = "rmsprop", metrics = ['mse'])
    trainingHistory = modell.fit(X_train, Y_train, validation_split=0.16, epochs=1000, batch_size = len(X_train), callbacks=callback_list, verbose=0)
    
    return trainingHistory, modell


def neuralNetwork1x1DoubleTrainableLayer(X_train, Y_train, fold, num, oldModel):
    
    from keras.optimizers import RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2
        
    #save modell
    filepath="model\m1x1Control_Fold" + str(fold) + "_Num" + str(num) + ".hdf5"
    
    #Extract the layer from the oldModel object
    trainableLayer = oldModel.get_layer(name="inputLayer")
    trainableLayer2 = oldModel.get_layer(name="hiddenLayer1")
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'val_loss', min_delta = 0, verbose = 0, patience = 50, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'val_loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and instantization of freezedLayer
    modell = Sequential()
    modell.add(trainableLayer)
    modell.add(trainableLayer2)
    modell.add(Dense(len(Y_train[0]), init = "he_normal", activation = 'linear'))    
    modell.compile(loss = "mean_squared_error", optimizer = "rmsprop", metrics = ['mse'])
    trainingHistory = modell.fit(X_train, Y_train, validation_split=0.16, epochs=1000, batch_size = len(X_train), callbacks=callback_list, verbose=0)
    
    return trainingHistory, modell


def neuralNetwork1x1WholeModel(X_train, Y_train, fold, num, oldModel):
    
    from keras.optimizers import RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2
        
    #save modell
    filepath="model\m1x1Control_Fold" + str(fold) + "_Num" + str(num) + ".hdf5"
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'val_loss', min_delta = 0, verbose = 0, patience = 50, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'val_loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and instantization of freezedLayer
    modell = oldModel
    trainingHistory = modell.fit(X_train, Y_train, validation_split=0.16, epochs=1000, batch_size = len(X_train), callbacks=callback_list, verbose=0)
    
    return trainingHistory, modell


def neuralNetworkAllConfigurations(X_complete, Y_complete):
    
    from keras.optimizers import RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2, l1
        
    #save modell
    filepath="model\m1x1" +str(456) + ".hdf5"
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'val_loss', min_delta = 0, verbose = 0, patience = 50, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'val_loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and HyperParameter Tuning -- Number Hidden Layers and Neurons per layer
    modell = Sequential()
    modell.add(Dense(len(X_complete[0]), init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='inputLayer'))
    modell.add(Dense(7, init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='hiddenLayer1'))
    modell.add(Dense(len(Y_complete[0]), init = "he_normal", activation = 'linear'))    
    modell.compile(loss = "mean_squared_error", optimizer = "rmsprop", metrics = ['mse'])
    trainingHistory = modell.fit(X_complete, Y_complete, validation_split=0.16, epochs=1000, batch_size = len(X_complete), callbacks=callback_list, verbose=0)
    
    return trainingHistory, modell

def neuralNetwork1x1Measures(X_complete, Y_complete):
    
    from keras.optimizers import RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2
        
    #save modell
    filepath="model\m1x1Measures" +str(456) + ".hdf5"
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'loss', min_delta = 0, verbose = 0, patience = 50, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and HyperParameter Tuning -- Number Hidden Layers and Neurons per layer
    modell = Sequential()
    modell.add(Dense(len(X_complete[0]), init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='inputLayer'))
    modell.add(Dense(7, init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='hiddenLayer1'))
    modell.add(Dense(len(Y_complete[0]), init = "he_normal", activation = 'linear'))    
    modell.compile(loss = "mean_squared_error", optimizer = "rmsprop", metrics = ['mse'])
    trainingHistory = modell.fit(X_complete, Y_complete, validation_split=0, epochs=1000, batch_size = len(X_complete), callbacks=callback_list, verbose=0)
    
    return trainingHistory, modell

def neuralNetwork1x1Length(X_complete, Y_complete):
    
    from keras.optimizers import RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2
        
    #save modell
    filepath="model\m1x1Length" +str(456) + ".hdf5"
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'loss', min_delta = 0, verbose = 0, patience = 50, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and HyperParameter Tuning -- Number Hidden Layers and Neurons per layer
    modell = Sequential()
    modell.add(Dense(len(X_complete[0]), init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='inputLayer'))
    modell.add(Dense(7, init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='hiddenLayer1'))
    modell.add(Dense(len(Y_complete[0]), init = "he_normal", activation = 'linear'))    
    modell.compile(loss = "mean_squared_error", optimizer = "rmsprop", metrics = ['mse'])
    trainingHistory = modell.fit(X_complete, Y_complete, validation_split=0, epochs=1000, batch_size = len(X_complete), callbacks=callback_list, verbose=0)
    
    return trainingHistory, modell

def neuralNetwork1x1Angle(X_complete, Y_complete):
    
    from keras.optimizers import RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2
        
    #save modell
    filepath="model\m1x1Angle.hdf5"
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'loss', min_delta = 0, verbose = 0, patience = 50, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and HyperParameter Tuning -- Number Hidden Layers and Neurons per layer
    modell = Sequential()
    modell.add(Dense(len(X_complete[0]), init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='inputLayer'))
    modell.add(Dense(7, init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='hiddenLayer1'))
    modell.add(Dense(len(Y_complete[0]), init = "he_normal", activation = 'linear'))    
    modell.compile(loss = "mean_squared_error", optimizer = "rmsprop", metrics = ['mse'])
    trainingHistory = modell.fit(X_complete, Y_complete, validation_split=0, epochs=1000, batch_size = len(X_complete), callbacks=callback_list, verbose=0)
    
    return trainingHistory, modell

def neuralNetwork1x1Roundness(X_complete, Y_complete):
    
    from keras.optimizers import RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2
        
    #save modell
    filepath="model\m1x1Roundness.hdf5"
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'loss', min_delta = 0, verbose = 0, patience = 50, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and HyperParameter Tuning -- Number Hidden Layers and Neurons per layer
    modell = Sequential()
    modell.add(Dense(len(X_complete[0]), init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='inputLayer'))
    modell.add(Dense(7, init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='hiddenLayer1'))
    modell.add(Dense(len(Y_complete[0]), init = "he_normal", activation = 'linear'))    
    modell.compile(loss = "mean_squared_error", optimizer = "rmsprop", metrics = ['mse'])
    trainingHistory = modell.fit(X_complete, Y_complete, validation_split=0, epochs=1000, batch_size = len(X_complete), callbacks=callback_list, verbose=0)
    
    return trainingHistory, modell

def neuralNetwork1x1Width(X_complete, Y_complete):
    
    from keras.optimizers import RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2
        
    #save modell
    filepath="model\m1x1Width.hdf5"
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'loss', min_delta = 0, verbose = 0, patience = 50, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and HyperParameter Tuning -- Number Hidden Layers and Neurons per layer
    modell = Sequential()
    modell.add(Dense(len(X_complete[0]), init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='inputLayer'))
    modell.add(Dense(7, init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='hiddenLayer1'))
    modell.add(Dense(len(Y_complete[0]), init = "he_normal", activation = 'linear'))    
    modell.compile(loss = "mean_squared_error", optimizer = "rmsprop", metrics = ['mse'])
    trainingHistory = modell.fit(X_complete, Y_complete, validation_split=0, epochs=1000, batch_size = len(X_complete), callbacks=callback_list, verbose=0)
    
    return trainingHistory, modell

def neuralNetwork1x1Heighth(X_complete, Y_complete):
    
    from keras.optimizers import RMSprop
    from keras.models import Sequential
    from keras.layers import Dense
    from keras.callbacks import EarlyStopping
    from keras.callbacks import ModelCheckpoint
    from keras.regularizers import l2
        
    #save modell
    filepath="model\m1x1Heighth.hdf5"
    
    #callbacks
    stop_here_please = EarlyStopping(monitor = 'loss', min_delta = 0, verbose = 0, patience = 50, mode = 'auto')
    checkpoint = ModelCheckpoint(filepath, monitor = 'loss', verbose = 0, save_best_only = True, mode = 'min')
    callback_list = [checkpoint,stop_here_please]
    
    #Model and HyperParameter Tuning -- Number Hidden Layers and Neurons per layer
    modell = Sequential()
    modell.add(Dense(len(X_complete[0]), init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='inputLayer'))
    modell.add(Dense(7, init = "he_normal", W_regularizer = l2(0.01), input_dim = len(X_complete[0]), activation = "tanh", name='hiddenLayer1'))
    modell.add(Dense(len(Y_complete[0]), init = "he_normal", activation = 'linear'))    
    modell.compile(loss = "mean_squared_error", optimizer = "rmsprop", metrics = ['mse'])
    trainingHistory = modell.fit(X_complete, Y_complete, validation_split=0, epochs=1000, batch_size = len(X_complete), callbacks=callback_list, verbose=0)
    
    return trainingHistory, modell